// It creates empty SmallCube object.
// This class is a dependency for the SmallCubeInitializer.
function SmallCubeFactory(cubeCoordinatesLoader)
{
	return {
		// Creates small cube.
		create: function()
		{
			return new SmallCube(cubeCoordinatesLoader);
		},
	};
}