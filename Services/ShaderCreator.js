function ShaderCreator()
{
	return {
		// creates and returns a new shader
		createShader: function(gl, id) {
			var script = document.getElementById(id);
			if (!script)
			{
				return null;
			}
			
			var str = "";
			var k = script.firstChild;
			while (k)
			{
				if (k.nodeType == 3)
				{
					str += k.textContent;
				}
				k = k.nextSibling;
			}

			var shader;
			var type = script.getAttribute("type");
			if (type == "x-shader/x-fragment")
			{
				shader = gl.createShader(gl.FRAGMENT_SHADER);
			}
			else if (type == "x-shader/x-vertex")
			{
				shader = gl.createShader(gl.VERTEX_SHADER);
			}
			else
			{
				return null;
			}
			
			gl.shaderSource(shader, str);
			gl.compileShader(shader);
			
			if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
				return null;
				
			return shader;
		}
	}
}