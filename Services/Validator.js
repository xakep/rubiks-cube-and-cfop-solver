function Validator()
{
	// stickers array
	/*
	0 is a frame
	[ 1..21]: 3 2 3 2 3 2 3 2 1
	[22..34]: 2 1 2 1 2 1 2 1
	[35..54]: 3 2 3 2 3 2 3 2 1
	centers are: 21, 24, 27, 30, 33, 54, ordered: [54, 21, 27, 33, 30, 24]
	order is important to easily find the opposite
	*/

	// these are start point in the corresponfing interval, begisns with 1 (o is a frame)
	// 1 2 3, 4 5, 6 7 8, 9 10, 11 12 13, 14 15, 16 17 18, 19 20, 21 - back layer
	// 22 23, 24, 25 26, 27, 28 29, 30, 31 32, 33 - middle layer
	// 34 35 36, 37 38, 39 40 41, 42 43, 44 45 46, 47 48, 49 50 51, 52 53, 54 - front layer
	// 
	const centers = [ 54, 21, 27, 33, 30, 24 ]; // +0, no adjacents
	const edges = [ 4, 9, 14, 19, 22, 25, 28, 31, 37, 42, 47, 52 ]; // +1 for each
	const corners = [ 1, 6, 11, 16, 34, 39, 44, 49 ]; // +2 for each

	const EDGE = 0;
	const CORNER = 1;
	const CENTER = 2;

	const COLORS_NAMES = ["", "Red", "Orange", "Yellow", "White", "Green", "Blue"];

	// checks whether specified color can be set
	// number is [0..53] (this is NOT number of cube, it's number of sticker)
	var colorCanBeApplied = function(stickers, number, colorIndex, setOppositeColor) {
		if (colorIndex == 0)
			return true; // skip gray colors, used when cell should be clear

		// 1. each color can be used 9 times
		// count specified color
		var colorUsed = 0;
		for (var i = 1; i < stickers.length; i++)
		{
			if (stickers[i].colorIndex == colorIndex)
			{
				colorUsed++;
				if (colorUsed == 9) 
				{
					console.log("9 stickers already have this color");
					return false; // 9 stickers already use this color
				}
			}
		}

		var type = getStickerType(number);

		if (type == CENTER)
		{
			// center color should be unique
			for (var i = 0; i < centers.length; i++)
			{
				if (centers[i] == number)
					continue; // skip current

				if (stickers[ centers[i] ].colorIndex == colorIndex)
				{
					console.log("The center with this color is already exist");
					return false;
				}
			}

			// if opposite center is set, only one color is allowed
			var oppositeCenter = getOppositeCenter(number);

			var oppositeCenterColor = stickers[oppositeCenter].colorIndex;
			if (oppositeCenterColor > 0)
			{
				var oppositeColor = oppositeCenterColor % 2 == 0
					? oppositeCenterColor - 1
					: oppositeCenterColor + 1;

				if (colorIndex != oppositeColor)
				{
					console.log("This sticker should be " + COLORS_NAMES[oppositeColor]);
					return false;
				}
			}
			else
			{
				// if opposite color is not set, set it
				var color = colorIndex % 2 == 0
					? colorIndex - 1
					: colorIndex + 1;
				setOppositeColor(oppositeCenter, color);
			}

			return true;
		}

		// 2. one cube can't use same colors on different sides
		
		if (type == EDGE)
		{
			// 2.1 edges should have different colors
			// if adjacent color is not set continue
			var adj = getAdjacentEdge(number);
			var adjColor = stickers[adj].colorIndex;

			if (adjColor != 0)
			{
				if (adjColor == colorIndex)
				{
					console.log("The edge can not have the same colors");
					return false;
				}
			}

			// 2.2 edge can't have opposite colors on stickers, for example red and orange
			if (adjColor != 0) // if adj has color
			{
				var oppositeColor = adjColor % 2 == 0
					? adjColor - 1
					: adjColor + 1;
				if (colorIndex == oppositeColor)
				{
					console.log("This color is opposite for adjacent sticker's one");
					return false;
				}
			}

			// 2.3 only 4 edges can have same color
			var count = 0;
			for (var i = 0; i < edges.length; i++)
			{
				if (stickers[ edges[i] ].colorIndex == colorIndex || stickers[ edges[i] + 1 ].colorIndex == colorIndex)
				{
					if (++count == 4)
					{
						console.log("4 edges already have this color");
						return false;
					}
				}
			}

			// 2.4 check for similar initialized edges
			if (adjColor != 0) // if adj has color
			{
				var currentEdgeColors = [colorIndex, adjColor];

				// check whether the egde with same colors
				for (var i = 0; i < edges.length; i++)
				{
					if (edges[i] == number || edges[i] + 1 == number)
						continue; // this is the current edge, skip it

					if (stickers[edges[i]].colorIndex > 0 && stickers[edges[i] + 1].colorIndex > 0) // checked edge has both colors
					{
						var otherEdgeColors = [stickers[edges[i]].colorIndex, stickers[edges[i] + 1].colorIndex];

						if (compareArrays(currentEdgeColors, otherEdgeColors))
						{
							console.log("There is an edge with same colors");
							return false; // found the same edge
						}
					}
				}
			}

			return true;
		}

		if (type == CORNER)
		{
			// corner's stickers should have different colors
			// if one of adjacent colors is not set continue
			var adj = getAdjacentStickers(number);
			if (adj[0] > 0 && adj[1] > 0)
			{
				for (var i = 0; i < 2; i++)
				{
					var adjColor = stickers[adj[i]].colorIndex;
					if (adjColor == colorIndex)
					{
						console.log("The corner can not have the same colors");
						return false;
					}
				}
			}

			// corner can't have opposite colors on stickers, for example red and orange
			if (adj[0] > 0 || adj[1] > 0)
			{
				for (var i = 0; i < 2; i++) // check 2 adjacents
				{
					var adjColor = stickers[adj[i]].colorIndex;
					if (adjColor > 0)
					{
						var oppositeColor = adjColor % 2 == 0
							? adjColor - 1
							: adjColor + 1;
						if (colorIndex == oppositeColor)
						{
							console.log("This color is opposite for adjacent sticker's one");
							return false;
						}
					}
				}
			}

			// only 4 corners can have same color
			var count = 0;
			for (var i = 0; i < corners.length; i++)
			{
				for (var y = 0; y < 3; y++)
				{
					if (stickers[corners[i] + y].colorIndex == colorIndex)
					{
						if (++count == 4)
						{
							console.log("4 corners already have this color");
							return false;
						}
						else
							break; // skip other adjacents for this corner
					}
				}
			}

			// check for similar initialized corners
			var adjColors = [stickers[adj[0]].colorIndex, stickers[adj[1]].colorIndex];
			if (adjColors[0] > 0 && adjColors[1] > 0) // if both adj have color
			{
				var currentCornerColors = [colorIndex, adjColors[0], adjColors[1]];

				// check whether the corner with same colors
				for (var i = 0; i < corners.length; i++)
				{
					if (corners[i] == number || corners[i] + 1 == number || corners[i] + 2 == number)
						continue; // this is the current corner, skip it

					// if checked corner has all all colors
					if (stickers[corners[i]].colorIndex > 0 && stickers[corners[i] + 1].colorIndex > 0 && stickers[corners[i] + 2].colorIndex > 0)
					{
						var otherCornerColors = [stickers[corners[i]].colorIndex, stickers[corners[i] + 1].colorIndex, stickers[corners[i] + 2].colorIndex];

						if (compareArrays(currentCornerColors, otherCornerColors))
						{
							console.log("There is an corner with same colors");
							return false; // found the same corner
						}
					}
				}
			}

			// corners must have specific color order
		}

		return true;
	}

	// returns type of sticker
	// can be:
	// 0 - edge
	// 1 - corner
	// 2 - center
	var getStickerType = function(sideNumber) {
		for (var i = 0; i < centers.length; i++)
		{
			if (centers[i] == sideNumber)
				return CENTER;
		}

		for (var i = 0; i < edges.length; i++)
		{
			if (edges[i] == sideNumber || edges[i] + 1 == sideNumber)
				return EDGE;
		}

		for (var i = 0; i < corners.length; i++)
		{
			if (corners[i] == sideNumber || corners[i] + 1 == sideNumber || corners[i] + 2 == sideNumber)
				return CORNER;
		}
	}

	// returns adjacent edge number for specified one
	var getAdjacentEdge = function(number) {
		for (var i = 0; i < edges.length; i++)
		{
			if (edges[i] == number || edges[i] + 1 == number)
			{
				return edges[i] == number
					? edges[i] + 1
					: edges[i];
			}
		}
	}

	// returns adjacent stickers for corner
	var getAdjacentStickers = function(number) {
		for (var i = 0; i < corners.length; i++)
		{
			var result = [];

			if (number >= corners[i] && number <= corners[i] + 2)
			{
				for (var y = 0; y < 3; y++)
				{
					if (corners[i] + y == number)
						continue;

					result.push(corners[i] + y);
				}

				return result;
			}
		}
	}

	// returns index in [1..54]
	var getOppositeCenter = function(number) {
		var currentCenterIndex = 0;
		for (var i = 0; i < centers.length; i++)
		{
			if (centers[i] == number)
				break;
			currentCenterIndex++;
		}

		return currentCenterIndex % 2 == 0
			? centers[currentCenterIndex + 1]
			: centers[currentCenterIndex - 1];
	}

	// each arrays has 2 or 3 integers
	var compareArrays = function(a1, a2) {
		a1.sort();
		a2.sort();
		for (var i = 0; i < a1.length; i++)
		{
			if (a1[i] != a2[i])
				return false;
		}

		return true;
	}

	return {
		isValid: function(stickers, number, colorIndex, setOppositeColor) {
			return colorCanBeApplied(stickers, number, colorIndex, setOppositeColor);
		}
	}
}