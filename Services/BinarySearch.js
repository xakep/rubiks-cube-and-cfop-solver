function BinarySearch()
{
	var start = 0;
	var _items;
	var end = 0;
	
	// how many times recursive function was called (for tests purposes)
	var invoked = 0;
	
	// compares two arrays
	// each array has 2 integer elements
	var isFirstArgGreaterThanSecondOne = function(a1, a2)
	{
		if ( a1[0] > a2[0] || (a1[0] == a2[0] && a1[1] > a2[1]) )
			return true;
		else
			return false;
	};
	
	// finds among arrays
	var recursiveFind = function(find, start, end)
	{
		invoked++;
		var chunkSize = 1 + (end - start);
 
		if (chunkSize == 0)
			return -1;
	 
		var midpoint = start + (Math.floor(chunkSize / 2));
		if (_items[midpoint][0] == find[0] && _items[midpoint][1] == find[1])
		{
			return _items[midpoint][2];
		}
		else if (isFirstArgGreaterThanSecondOne(_items[midpoint], find))
		{
			return recursiveFind(find, start, midpoint - 1);
		}
		else
		{
			return recursiveFind(find, midpoint + 1, end);
		}
	};
	
	// finds among integers
	var recursiveFindInt = function(find, start, end)
	{
		invoked++;
		var chunkSize = 1 + (end - start);
 
		if (chunkSize == 0)
			return -1;
	 
		var midpoint = start + (Math.floor(chunkSize / 2));
		if (_items[midpoint] == find)
		{
			return midpoint; // returns position in the items array
		}
		else if (_items[midpoint] > find)
		{
			return recursiveFindInt(find, start, midpoint - 1);
		}
		else
		{
			return recursiveFindInt(find, midpoint + 1, end);
		}
	};
	 
	return {
		// initializes
		init: function(items) {
			_items = items;
			end = _items.length - 1;
		},
		
		// finds item
		indexOf: function(item)
		{
			if (typeof(item) != "number")
				return recursiveFind(item, start, end);
			else
				return recursiveFindInt(item, start, end);
		},
		
		// only for tests
		invokedTimes: function()
		{
			return invoked;
		}
	};
}