function PLL(pllChecher, rotationService, mappingService)
{
	var _pllChecker = pllChecher;
	var _mappingService = mappingService;
	var _rotationService = rotationService;

	return {
		solvePLL: function(cubeColors) {
			var moves = -1;

			var rotatedCubeColors = null;

			for (var i = 0; i < 4; i++)
			{
				moves = _pllChecker.checkPLL(rotatedCubeColors == null ? cubeColors : rotatedCubeColors);

				if (moves == -1)
				{
					// rotate
					rotatedCubeColors = _rotationService.applyMoves(
						rotatedCubeColors == null ? cubeColors : rotatedCubeColors, [6]);
				}
				else
				{
					if (i > 0) moves = _mappingService.mappedMoves(moves, i);
					break;
				}
			}

			return new StageSolution(
				moves,
				_rotationService.applyMoves(cubeColors, moves));
		}
	}
}