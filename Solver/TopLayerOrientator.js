// this is last stage
// orientates top layer after PLL stage
function TopLayerOrientator(constantsService)
{
	var _constantsService = constantsService;

	return {
		// returns moves needed for orientate top layer
		orientate: function(cubeColors) {
			var frontCenterColor = cubeColors.getSideColor(Z, F11);
			var topLayerCenterColor = cubeColors.getSideColor(Z, F12);

			if (topLayerCenterColor == frontCenterColor)
			{
				return []; // already oriented correctly
			}

			if (topLayerCenterColor == _constantsService.getOppositeColor(frontCenterColor))
			{
				return [6, 6]; // 180
			}

			if (frontCenterColor != cubeColors.getSideColor(X, M22))
			{
				return [6]; // front center = left side
			}
			else
			{
				return [-6]; // front center = right side
			}
		}
	}
}