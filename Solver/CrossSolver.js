function CrossSolver(rotationService, crossSolverHelper, mappingService)
{
	var _rotationService = rotationService;
	var _crossSolverHelper = crossSolverHelper;
	var _mappingService = mappingService;
	
	var check = function(lastMove, nextMoves) {
		if (nextMoves[0] + lastMove == 0)
			return nextMoves.length - 2;
			
		return nextMoves.length;
	}
	
	return {
		orientate: function(cubeColors) {
			// bottom center color
			var bottomCenterColor = cubeColors.getSideColor(Y, M10);
			
			var bottomEdges = [F10, M00, B10, M20];
			
			var centerColors = [
				cubeColors.getSideColor(Z, F11), // front
				cubeColors.getSideColor(X, M01), // left
				cubeColors.getSideColor(Z, B11), // back
				cubeColors.getSideColor(X, M21)
			];
			
			var bottomColors = [-1, -1, -1, -1];
			
			for (var i = 0; i < 4; i++)
			{
				if (cubeColors.getSideColor( Y, bottomEdges[i] ) == bottomCenterColor)
					bottomColors[i] = cubeColors.getSideColor( i % 2 == 0 ? Z : X, bottomEdges[i] );
			}
				
			var correctCubesCount = [];
				
			for (var i = 0; i < 4; i++) // 4 shifts
			{
				var hits = 0;
				for (var y = 0; y < 4; y++) // 4 cubes to check
				{
					if (centerColors[y] == bottomColors[y]) hits++;
				}
				
				correctCubesCount[i] = hits;
				
				// shift bottoms
				bottomColors.push( bottomColors.shift() );
			}
			
			var max = 0;
			
			for (var i = 1; i < 4; i++)
			{
				if (correctCubesCount[i] > correctCubesCount[max])
				{
					max = i;
				}
			}
			
			switch (max)
			{
				case 0: return [];
				case 1: return [4];
				case 2: return correctCubesCount[2] == correctCubesCount[3] ? [-4] : [4, 4];
				case 3: return [-4];
			}
		},
		
		// cubeColors is original not oriented yet cube
		// orientationMoves are moves to orientate the cube, can be empty
		// returns moves required to solve cross
		solveCross: function(cubeColors, orientationMoves) {
			
			var resultMoves = [];
			
			// if previous step returned some
			// cube should be updated
			if (orientationMoves.length > 0)
				cubeColors = _rotationService.applyMoves(cubeColors, orientationMoves);
				
			var correctCubes = _crossSolverHelper.getCorrectOrientedCubesCount(cubeColors);

			for (var i = 0; i < 4 - correctCubes; i++) // check incorrect placed cubes
			{
				var moves = [];
				var minMoves = -1; // [0..3]
				
				// rotate cube 4 times to find out
				// which cube requires minimum moves
				for (var y = 0; y < 4; y++)
				{
					var a = _crossSolverHelper.findAlgorithm(cubeColors); // [-3, 3] for instance, can be []
					moves.push(a);
					
					if (minMoves == -1 && a.length != 0) // minMoves = -1 only when y = 0 or if no moves needed
					{
						minMoves = y;
						// moves = a;
					}
					else if (a.length != 0 && a.length < moves[minMoves].length)
					{
						minMoves = y; // set current side as min
					}
					/*else
					{
						if (a.length != 0)
						{
							var current = check(resultMoves[resultMoves.length - 1], a);
							var previous = check(resultMoves[resultMoves.length - 1], moves[minMoves]); // test it y - 1 versus minMoves
							
							if (current < previous) minMoves = y;
						}
					}*/
					
					// can be optimized: if a.length < 2, apply and break
					
					cubeColors = _rotationService.rotateEntireCube(cubeColors); // ccw rotation by default
				}
				
				// now I know which cube should be replaced (minMoves required)
				// if minMoves == 0, no mapping required, otherwise map sides
				var applyThis = minMoves > 0
					? _mappingService.mappedMoves(moves[minMoves], minMoves)
					: moves[minMoves];
					
				cubeColors = _rotationService.applyMoves(cubeColors, applyThis);
				
				resultMoves = resultMoves.concat(applyThis);
			}
			
			return new StageSolution(resultMoves, cubeColors);
		}
	}
}
