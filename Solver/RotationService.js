// this class returns new CubeColors object every time
function RotationService(cube, rotationServiceHelper, cubeColorsFactory, layerService)
{
	var _cube = cube; // for layers
	var _rotationServiceHelper = rotationServiceHelper;
	var _cubeColorsFactory = cubeColorsFactory;
	var _layerService = layerService;

	// rotates specified layers
	var rotateLayers = function(colors, layersToRotateCount, plane, direction) {
		for (var i = 0; i < layersToRotateCount; i++)
		{
			var layerNumbers = _cube.getLayer(plane, i);
			var layerColors = [];
			// 1 is middle layer, it has no center cube
			var len = i == 1 ? layerNumbers.length : layerNumbers.length - 1;
			
			for (var y = 0; y < len; y++)
			{
				layerColors.push( colors[layerNumbers[y]] );
			}

			var rotatedLayer = _rotationServiceHelper.rotate(direction, layerColors, plane);
			
			// update colors and create new object via factory
			for (var y = 0; y < len; y++)
			{
				colors[layerNumbers[y]] = rotatedLayer[y];
			}
		}

		return _cubeColorsFactory.createCubeColors(colors);
	}
	
	return {
		// rotates bottom layer (0) once, CW
		// returns new CubeColors object
		rotateBottomLayer: function(cubeColors) {
			return rotateLayers(cubeColors.getColors().slice(), 1, Y, CW);
		},

		// rotates all layers once
		// if side is not defined, rotates around Y
		// side can be (1-5):
		// 1 - Front, 2 - Right, 3 - Back, 4 - Left, 5 - Top
		rotateEntireCube: function(cubeColors, side) {
			if (!side) // rotates all 3 Y layers CW, -4, -5, -6
				return rotateLayers(cubeColors.getColors().slice(), 3, Y, CW); // 3 means rotate all 3 layers
			else
			{
				switch (side)
				{
					case 1:
						return rotateLayers(cubeColors.getColors().slice(), 3, X, CW);

					case 2:
						return rotateLayers(cubeColors.getColors().slice(), 3, Z, CCW);;

					case 3:
						return rotateLayers(cubeColors.getColors().slice(), 3, X, CCW);

					case 4:
						return rotateLayers(cubeColors.getColors().slice(), 3, Z, CW);;

					case 5:
						var oneRotation = rotateLayers(cubeColors.getColors().slice(), 3, X, CW);
						return rotateLayers(oneRotation.getColors().slice(), 3, X, CW);
				}
			}
		},
		
		// applies the specified moves to the cube
		applyMoves: function(cubeColors, moves) {
			var colors = cubeColors.getColors().slice();
			
			for (var i = 0; i < moves.length; i++)
			{
				var move = Math.abs(moves[i]) - 1;

				var plane = _layerService.getPlane(move);
				var layerNumbers = _cube.getLayer(plane, _layerService.getNumber(move));
				
				var layerColors = [];
				for (var y = 0; y < layerNumbers.length; y++)
				{
					layerColors.push(colors[layerNumbers[y]]);
				}

				var rotatedLayer = _rotationServiceHelper.rotate(
					moves[i] > 0 ? CW : CCW,
					layerColors,
					plane);
					
				// update initial cubeColors
				for (var y = 0; y < rotatedLayer.length; y++)
				{
					colors[layerNumbers[y]] = rotatedLayer[y];
				}
			}
			
			// construct new CubeColors and return it
			return _cubeColorsFactory.createCubeColors(colors);
		}
	}
}