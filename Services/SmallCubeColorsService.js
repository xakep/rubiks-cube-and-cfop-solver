function SmallCubeColorsService(constantsService)
{
	var outers = 0;

	// generates colors
	// returns array [x, y, z]
	var generate = function(coordinates, initialColors)
	{
		var colors = [0, 0, 0];
		
		for (var i = 0; i < 6; i++) // 6 sides per cube
		{
			var xyz = Math.floor(i / 2);
			
			if (isOuterSide(coordinates[xyz], i))
			{
				if (initialColors == null)
				{
					colors[xyz] = Math.floor(i % 2) == 0
						? i + 1 // +1 because 0th color is gray, see ConstantsService
						: -(i + 1);
				}
				else
				{
					colors[xyz] = Math.floor(i % 2) == 0
						? initialColors.shift()
						: -initialColors.shift();
				}
			}
		}
		Object.freeze(colors);

		return colors;
	}
	
	var updateOneSide = function(colors, side, color) {
		var result = [0, 0, 0];
		
		for (var i = 0; i < 3; i++)
		{
			if (side == i)
			{
				result[i] = colors[i] > 0
					? color
					: -color;
			}
			else
				result[i] = colors[i];
		}
		
		return result;
	}
	
	// determines whether the specified side is outer for entire cube
	var isOuterSide = function(coordinate, sideNumber)
	{
		var halfCube = constantsService.getHalfCube() - (constantsService.isCubeEven() ? 0.5 : 0);
			
		if (Math.abs(coordinate) == halfCube)
		{
			var sign = sideNumber % 2 == 0 ? 1 : -1;
			if ( (coordinate > 0 && sign > 0) || (coordinate < 0 && sign < 0) )
				return true;
		}
		
		return false;
	}
	
	// creates colors buffer for one side
	var getColorAsBuffer = function(colorNumber) {
		var verticesPerSide = constantsService.getVerticesPerSide(); // 4 or 8
		var color = constantsService.getColor(colorNumber);
		
		buffer = [];
		for (var y = 0; y < verticesPerSide * 4; y += 4)
		{
			buffer = buffer.concat(color);
		}

		return buffer;
	}
	
	// generates colors buffer
	var createBuffer = function(colors) {
		var buffer = [];
		
		var grayColor = getColorAsBuffer(0);
		
		for (var i = 0; i < 6; i++) // 6 sides per cube
		{
			var side = Math.floor(i / 2); // 0, 1 or 2
			var sideColor = colors[side]; // color can be 0, - or +
			var sign = Math.floor(i % 2); // 0 or 1, 0 -> +, 1 -> -

			if (sideColor == 0 || (sideColor < 0 && sign == 0) || (sideColor > 0 && sign == 1)) // 0 -> +; 1 -> -
			{
				buffer = buffer.concat(grayColor); // gray
			}
			else
			{
				buffer = buffer.concat(getColorAsBuffer(Math.abs(sideColor)));
			}
		}
		Object.freeze(buffer);

		return buffer;
	}
	
	var getNewColors = function(original, newOne, rotationPlane) {
		// get old colors
		var oldColors = original.getColors();

		// get old signs
		var signs = [];
		for (var i = 0; i < oldColors.length; i++)
		{
			if (rotationPlane == i)
				continue;
				
			if (oldColors[i] == 0)
				signs.push(0);
			else
				signs.push(oldColors[i] / Math.abs(oldColors[i]));
		}
		
		// get new colors
		var newColors = newOne.getColors();
		
		var result = [0, 0, 0];
		result[rotationPlane] = newColors[rotationPlane];
		
		switch (rotationPlane)
		{
			case 0:
				result[1] = Math.abs(newColors[2]) * signs[0];
				result[2] = Math.abs(newColors[1]) * signs[1];
				break;
			case 1:
				result[0] = Math.abs(newColors[2]) * signs[0];
				result[2] = Math.abs(newColors[0]) * signs[1];
				break;
			case 2:
				result[0] = Math.abs(newColors[1]) * signs[0];
				result[1] = Math.abs(newColors[0]) * signs[1];
				break;
		}
		
		return result;
	}
	
	return {
		// creates a new SmallCubeColors object from coordinates
		// colors - array of 1, 2 or 3 integers (depends on how much stickers this small cube has)
		createColorsObject: function(coords, initialColors) {
			var colors = generate(coords, initialColors);
			return new SmallCubeColors(colors, createBuffer(colors));
		},
		
		// creates a new object from another one
		// 'originalObject' is a colors of cube which will be replaced
		// by 'anotherObject' when plane is rotated
		createColorsObjectFrom: function(originalObject, anotherObject, rotatingPlane) {
			var newColors = getNewColors(originalObject, anotherObject, rotatingPlane);
			var newBuffer = createBuffer(newColors);

			return new SmallCubeColors(newColors, newBuffer);
		},
		
		// updates specified side color
		updateSideColor: function(colors, side, color) {
			var colors = updateOneSide(colors, side, color);
			return new SmallCubeColors(colors, createBuffer(colors));
		}
	}
}