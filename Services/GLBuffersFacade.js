// Regenerate huge buffer after each rotation
// and holds generated GL buffers
function GLBuffersFacade(jsBuffersFactory, glBuffersFactory)
{
	// dependencies
	var _jsBuffersFactory = jsBuffersFactory;
	var _glBuffersFactory = glBuffersFactory;
	
	// GL buffers
	var rotatingGLVertices;
	var staticGLVertices;
	var rotatingGLColors;
	var staticGLColors;
	var rotatingGLIndices;
	var staticGLIndices;
	var rotatingGLNormals;
	var staticGLNormals;

	var asyncStaticJSColors = [];
	var asyncRotatingJSColors = [];
	
	// magic numbers
	var ROTATING_VERTICES = 1000;
	var STATIC_VERTICES = 1001;
	var ROTATING_COLORS = 1002;
	var STATIC_COLORS = 1003;
	var ROTATING_INDICES = 1004;
	var STATIC_INDICES = 1005;
	var ROTATING_NORMALS = 1006;
	var STATIC_NORMALS = 1007;

	var frameAndStickers = null;
	var sceneUniqColors = null;

	var validator = null;

	var generateFrameAndStickers = function() {
		sceneUniqColors = {};
		var blackColor = [0.0, 0.0, 0.0, 1.0];
		var blackKey = blackColor[0] + ':' + blackColor[1] + ':' + blackColor[2];
		sceneUniqColors[blackKey] = true; // black is unavailable, used by frame

		frameAndStickers = [];

		frameAndStickers.push(new SceneObject(
			_glBuffersFactory.createFloatBuffer(_jsBuffersFactory.createFrameVertices()),
			_glBuffersFactory.createUintBuffer(_jsBuffersFactory.getFrameIndices()),
			_glBuffersFactory.createFloatBuffer(_jsBuffersFactory.createFrameColors()),
			_glBuffersFactory.createFloatBuffer(_jsBuffersFactory.getFrameNormals()),
			blackColor // black for frame
		));

		var stickersVertices = _jsBuffersFactory.createStickersVertices();
		var stickersIndices = _jsBuffersFactory.getStickersIndices();
		var stickersColors = _jsBuffersFactory.createStickersColors();
		var stickersNormals = _jsBuffersFactory.getStickersNormals();

		for (var i = 0; i < 54; i++) // 54 = 9 * 6
		{
			frameAndStickers.push(new SceneObject(
				_glBuffersFactory.createFloatBuffer(stickersVertices[i]),
				_glBuffersFactory.createUintBuffer(stickersIndices[i]),
				_glBuffersFactory.createFloatBuffer(stickersColors[i]),
				_glBuffersFactory.createFloatBuffer(stickersNormals[i]),
				generateUniqColor()
			));
		}
	}

	// generates unique color (used as sticker label)
	var generateUniqColor = function()
	{
		var color = [Math.random(), Math.random(), Math.random(), 1.0];
	    var key = color[0] + ':' + color[1] + ':' + color[2];

	    if (key in sceneUniqColors)
	    {
	        return generateUniqColor();
	    }
	    else
	    {
	        sceneUniqColors[key] = true;
	        return color;
	    }
	}

	// this is callback for validator
	// when opposite center is not colored, this method is called from validator
	var setOppositeColor = function(oppositeIndex, color) {
		frameAndStickers[oppositeIndex].colorIndex = color;
		frameAndStickers[oppositeIndex].cbo = _glBuffersFactory.createFloatBuffer(
			_jsBuffersFactory.createStickerColors(color));
	}

	return {

		// generates static and rotating buffers
		generate: function(rotatingLayerNumber, rotateEntireCube, async) {
			async = false;
			if (async && asyncStaticJSColors.length == 0)
			{
				// when token is not match, do it synchronously
				async = false;
				console.error("Async operation was failed");
			}

			// get static vertices buffer
			var staticJSVerticesBuffer = _jsBuffersFactory.createStaticVerticesBuffer(rotatingLayerNumber);

			// get static colors buffer
			var staticJSColorsBuffer = async
				? asyncStaticJSColors.slice()
				: _jsBuffersFactory.createStaticColorsBuffer(rotatingLayerNumber);

			// get static normals buffer
			var staticJSNormalsBuffer = _jsBuffersFactory.createStaticNormalsBuffer(rotatingLayerNumber);

			// get static indices buffer
			var staticJSIndicesBuffer = _jsBuffersFactory.createStaticIndicesBuffer(rotatingLayerNumber);

			// get rotating vertices buffer
			var rotatingJSVerticesBuffer = _jsBuffersFactory.createRotatingVerticesBuffer(rotatingLayerNumber);
			
			// get rotating colors buffer
			var rotatingJSColorsBuffer = async
				? asyncRotatingJSColors.slice()
				: _jsBuffersFactory.createRotatingColorsBuffer(rotatingLayerNumber);

			// get rotating normals buffer
			var rotatingJSNormalsBuffer = _jsBuffersFactory.createRotatingNormalsBuffer(rotatingLayerNumber);

			// get rotating indices buffer
			var rotatingJSIndicesBuffer = _jsBuffersFactory.createRotatingIndicesBuffer(rotatingLayerNumber);
			
			if (!rotateEntireCube)
			{
				staticGLVertices = _glBuffersFactory.createFloatBuffer(staticJSVerticesBuffer);
				staticGLColors = _glBuffersFactory.createFloatBuffer(staticJSColorsBuffer);
				staticGLNormals = _glBuffersFactory.createFloatBuffer(staticJSNormalsBuffer);
				staticGLIndices = _glBuffersFactory.createUintBuffer(staticJSIndicesBuffer);
				rotatingGLVertices = _glBuffersFactory.createFloatBuffer(rotatingJSVerticesBuffer);
				rotatingGLColors = _glBuffersFactory.createFloatBuffer(rotatingJSColorsBuffer);
				rotatingGLNormals = _glBuffersFactory.createFloatBuffer(rotatingJSNormalsBuffer);
				rotatingGLIndices = _glBuffersFactory.createUintBuffer(rotatingJSIndicesBuffer);
			}
			else
			{
				staticGLVertices = _glBuffersFactory.createFloatBuffer([]);
				staticGLColors = _glBuffersFactory.createFloatBuffer([]);
				staticGLNormals = _glBuffersFactory.createFloatBuffer([]);
				staticGLIndices = _glBuffersFactory.createUintBuffer([]);

				rotatingGLVertices = _glBuffersFactory.createFloatBuffer(
					staticJSVerticesBuffer.concat(rotatingJSVerticesBuffer));

				rotatingGLColors = _glBuffersFactory.createFloatBuffer(
					staticJSColorsBuffer.concat(rotatingJSColorsBuffer));

				rotatingGLNormals = _glBuffersFactory.createFloatBuffer(
					staticJSNormalsBuffer.concat(rotatingJSNormalsBuffer));

				var staticVerticesCount = Math.floor(staticJSVerticesBuffer.length / 3);
				for (var i = 0; i < rotatingJSIndicesBuffer.length; i++)
				{
					staticJSIndicesBuffer.push(rotatingJSIndicesBuffer[i] + staticVerticesCount);
				}
				staticJSIndicesBuffer.numItems += rotatingJSIndicesBuffer.numItems;
				
				rotatingGLIndices = _glBuffersFactory.createUintBuffer(staticJSIndicesBuffer);
			}
		},
		
		// returns GL buffer by specified id
		getBuffer: function(id) {
			switch (id)
			{
				case ROTATING_VERTICES: return rotatingGLVertices;
				case STATIC_VERTICES: return staticGLVertices;
				
				case ROTATING_COLORS: return rotatingGLColors;
				case STATIC_COLORS: return staticGLColors;
				
				case ROTATING_INDICES: return rotatingGLIndices;
				case STATIC_INDICES: return staticGLIndices;
				
				case ROTATING_NORMALS: return rotatingGLNormals;
				case STATIC_NORMALS: return staticGLNormals;
			}
		},

		// returns buffer which represents cube frame (without 54 stickers)
		getFrameAndStickers: function(number) {
			if (frameAndStickers == null)
			{
				generateFrameAndStickers();
			}

			return frameAndStickers;
		},

		setAsyncGeenratedBuffers: function(staticColors, rotatingColors) {
			asyncStaticJSColors = staticColors;
			asyncRotatingJSColors = rotatingColors;
		},

		// set new color for specified sticker
		updateSceneObject: function(number, colorIndex) {
			if (validator == null)
			{
				validator = new Validator();
			}

			var currentObject = frameAndStickers[number];

			if (currentObject.colorIndex != colorIndex && validator.isValid(frameAndStickers, number, colorIndex, setOppositeColor))
			{
				currentObject.colorIndex = colorIndex;
				currentObject.cbo = _glBuffersFactory.createFloatBuffer(
					_jsBuffersFactory.createStickerColors(colorIndex));
			}
		},

		// checks whether all 54 stickers are colored
		areAllStickersSet: function() {
			for (var i = 1; i < frameAndStickers.length; i++)
			{
				if (frameAndStickers[i].colorIndex == 0)
					return false;
			}

			return true;
		}
	}
}
