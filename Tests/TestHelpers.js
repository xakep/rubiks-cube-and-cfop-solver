function TestHelpers()
{
	return {
		// creates array of arrays, each has [0, 0, 0]
		createArrayOfArrays: function(size)
		{
			var a = [];
			for (var i = 0; i < size; i++)
			{
				a.push([0, 0, 0]);
			}
			
			return a;
		},
		
		// creates array with specified size and initialize it
		// with numbers starting from 0
		createArray: function(size)
		{
			var a = [];
			for (var i = 0; i < size; i++)
			{
				a.push(i);
			}
			
			return a;
		},
		
		// creates array with identical number
		createArrayWith: function(arg, size)
		{
			var a = [];
			for (var i = 0; i < size; i++)
			{
				a.push(arg);
			}
			
			return a;
		},
		
		// creates mock colors buffer
		createColorsBuffer: function(colors)
		{
			var a = [];
			for (var i = 0; i < 6; i++)
			{
				for (var y = 0; y < 16; y++)
				{
					a.push(colors[i]);
				}
			}
			
			return a;
		}
	}
}