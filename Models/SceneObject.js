function SceneObject(vbo, ibo, cbo, nbo, uniqColor)
{
	this.vbo = vbo;
	this.ibo = ibo;
	this.cbo = cbo;
	this.nbo = nbo;
	this.uniqColor = uniqColor;
	this.colorIndex = 0; // gray by default
}