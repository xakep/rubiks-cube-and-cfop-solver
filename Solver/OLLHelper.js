function OLLHelper()
{
	// top layer cubes, except the center
	const topLayerCubes = [F22, F12, F02, M02, B02, B12, B22, M22];

	// http://www.speedsolving.com/wiki/index.php/OLL
	const algorithms = [
		[-9, -3, -6, 3, 6, 9, 7, -6, 1, 6, -1, -7],						// 2
		[-3, -6, -7, -3, 7, 3, 3, 6, 3, -9, -3, 9],						// 1
		[-3, -7, 3, 3, -9, 3, 3, 7, 3, 3, 9, -3],						// 49
		[-3, 7, 7, -1, -7, 1, -7, 3],									// 6
		[-3, -6, 3, -6, 3, -9, -3, 9, 6, 6, 3, -9, -3, 9],				// 17
		[3, 6, -3, 6, 3, -6, -3, -6, -3, -7, 3, 7],						// 36
		[-1, 6, 7, -6, 1, 6, -1, -7, 1],								// 31
		[-7, 6, 3, -6, -3, 7],											// 43
		[-3, 6, 6, 3, 3, 6, -3, 6, 3, 6, 6, -9, -3, 9],					// 55
		[3, 6, -3, 6, 3, -6, 9, -6, -9, -3],							// 52
		[-9, -3, -6, 3, 6, -3, -6, 3, 6, 9],							// 48
		[3, 9, 1, 9, -1, -9, 1, 9, -1, 9, 9, -3],						// 53
		[-9, -6, 9, 3, -9, -3, 6, 3, 9, -3],							// 10
		[1, -9, 3, -9, -3, 9, 9, -1],									// 7
		[3, 6, 3, -9, -3, 9, -6, -3],									// 46
		[-8, 6, -9, 3, 3, 9, 6, 7, 6, -7, 8],							// 4
		[-2, -6, -1, 7, 7, 1, -6, 3, -6, -3, 2],						// 3
		[-3, 7, -6, -7, -6, 3, 6, 6, 3, -9, -3, 9],						// 18
		[-3, -7, 3, 7, -6, 7, 6, -7],									// 37
		[2, -6, -3, -6, 3, 6, -2, 3, -9, -3, 9],						// 19
		[2, -6, 2, -9, 3, 9, 2, 2, -9, -3, 9],							// 20
		[2, -3, 7, -6, -7, 6, 3, -6, -2],								// 42
		[2, -6, -3, -6, 3, 6, 3, -9, -3, 9, -2],						// 29
		[2, -6, -2, 6, 6, 2, -6, -2],									// 28
		[-3, -7, -3, 7, 3, 3, 6, 6, -9, 3, 9, -3],						// 50
		[-1, 7, 7, -3, 7, 3, 7, 1],										// 5
		[-3, 7, -1, 7, -1, 4, 1, -4, 1, 7, 7, 3],						// 11
		[-9, -3, -6, 3, 6, -3, 9, 1, -9, 3, 9, -1],						// 56
		[-9, -6, -3, 6, 3, -6, -3, 6, 3, 9],							// 51
		[3, -9, -3, -6, 3, 9, -3, -9, 6, 9],							// 14
		[-1, -7, 1, 3, 6, -3, -6, -1, 7, 1],							// 15
		[-3, 7, 3, 1, -6, -1, 6, -3, -7, 3],							// 16
		[-9, -6, -3, 6, 6, 3, 6, -3, -6, 3, 9],							// 13
		[-3, -6, 3, 6, -7, 3, -9, -3, 9, 7],							// 34
		[-3, 6, 6, 3, 3, -9, -3, 9, -3, 6, 6, 3],						// 35
		[3, -9, -3, -6, 3, 6, 9, -6, -3],								// 40
		[1, -9, 3, -9, -3, 9, 3, -9, -3, 9, 9, -1],						// 54
		[9, -1, 6, 1, -6, -1, 6, 1, -6, -9],							// 47
		[3, 9, 1, 9, -1, 9, 9, -3],										// 8
		[3, 6, -3, 6, 3, -6, 3, -9, -3, 9, -6, -3],						// 9
		[-3, 6, -1, -6, 3, -6, 1, -6, -1, -6, 1],						// 22
		[-3, -6, 3, -6, -3, 6, 3, -6, -3, 6, 6, 3],						// 21
		[-1, -6, -3, 6, 1, -6, 3],										// 26
		[-3, 6, -1, -6, 3, 6, 1],										// 27
		[3, 6, 6, -3, -9, 6, 3, 6, -3, -6, 9],							// 23
		[-9, -3, 7, 3, 9, -3, -7, 3],									// 25
		[-8, 9, 6, -9, 6, 9, 6, 6, -9, 6, 8],							// 12
		[7, -6, 1, 6, -1, -7],											// 44
		[-3, -6, -7, 6, 3, -6, -3, 7, 3],								// 32
		[1, -6, -1, -6, 1, 6, -1, 6, -1, 7, 1, -7],						// 38
		[-9, -3, -6, 3, 6, 9],											// 45
		[-3, -6, 3, 6, 3, -9, -3, 9],									// 33
		[1, 9, -1, 6, 1, -6, -9, 6, -1],								// 39
		[2, -1, -7, 6, 7, -6, 1, 6, -2],								// 41
		[2, 6, -1, 6, 1, -6, 1, 9, -1, -9, -2],							// 30
		[-2, -7, -3, 7, 2, -7, 3, 7],									// 57
		[1, -9, 3, 9, -1, -9, -3, 9],									// 24
	];

	return {
		// gets Int number of OLL
		// how it works:
		// method gets binary code of current OLL
		// 		first 12 bits are side stickers, last 8 are from up side;
		// 		it begins from F22 cube, X sticker, and goes CW;
		// 		center cube is ignored.
		// returns binary parsed to int
		getAlgorithmNumber: function(cubeColors) {
			
			var bits = []; // new Bits();

			var topColor = cubeColors.getSideColor(Y, M12);

			for (var i = 0; i < topLayerCubes.length; i++)
			{
				if (i % 2 == 0) // corner: 0, 2, 4, 6
				{
					bits.unshift(cubeColors.getSideColor(i == 0 || i == 4 ? X : Z, topLayerCubes[i]) == topColor ? 1 : 0);
					bits.unshift(cubeColors.getSideColor(i == 0 || i == 4 ? Z : X, topLayerCubes[i]) == topColor ? 1 : 0);
				}
				else // edge: 1, 3, 5, 7
				{
					bits.unshift(cubeColors.getSideColor(i == 1 || i == 5 ? Z : X, topLayerCubes[i]) == topColor ? 1 : 0);
				}
			}

			for (var i = 0; i < topLayerCubes.length; i++)
			{
				bits.unshift(cubeColors.getSideColor(Y, topLayerCubes[i]) == topColor ? 1 : 0);
			}

			return parseInt(bits.join(""), 2);
		},

		// returns true if OLL is already solved
		// it happens sometimes that after f2l stage, oll is solved too automatically
		isOLLSolved: function(cubeColors) {
			var topColor = cubeColors.getSideColor(Y, M12);
			for (var i = 0; i < topLayerCubes.length; i++)
			{
				if (cubeColors.getSideColor(Y, topLayerCubes[i]) != topColor)
					return false;
			}

			return true;
		},

		// returns algorithm with specified number
		getMoves: function(number) {
			return algorithms[number];
		}
	}
}