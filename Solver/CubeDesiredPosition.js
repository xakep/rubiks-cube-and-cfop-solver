function CubeDesiredPosition(axis, degree)
{
	var _degree = degree;

	return {
		getAxis: function() {
			return axis;
		},

		// updates by 1
		updateDegree: function(sign) {
			if (sign == 0)
				return _degree;

			_degree += sign > 0 ? 1 : -1;

			return _degree;
		},

		getDegree: function() {
			return _degree;
		},
	}
}