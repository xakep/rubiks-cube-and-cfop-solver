function Picker()
{
	this.canvas = canvas;
	
	this.configure();
}

Picker.prototype.configure = function()
{
	var width = this.canvas.width;
	var height = this.canvas.height;
	
	// 1. Init Picking Texture
	this.texture = gl.createTexture();
	
	gl.bindTexture(gl.TEXTURE_2D, this.texture);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
	// next line prevents errors to console
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	
	// 2. Init Render Buffer
	this.renderBuffer = gl.createRenderbuffer();
	gl.bindRenderbuffer(gl.RENDERBUFFER, this.renderBuffer);
	gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, width, height);
	
	// 3. Init Frame Buffer
	this.frameBuffer = gl.createFramebuffer();
	gl.bindFramebuffer(gl.FRAMEBUFFER, this.frameBuffer);
	gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.texture, 0);
	gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, this.renderbuffer);

	// 4. Clean up
	gl.bindTexture(gl.TEXTURE_2D, null);
	gl.bindRenderbuffer(gl.RENDERBUFFER, null);
	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
}

Picker.prototype._compare = function(readOut, color)
{
	return (Math.abs(Math.round(color[0] * 255) - readOut[0]) <= 1 &&
			Math.abs(Math.round(color[1] * 255) - readOut[1]) <= 1 &&
			Math.abs(Math.round(color[2] * 255) - readOut[2]) <= 1);
}

Picker.prototype.find = function(coords, sceneObjects)
{	
	// read one pixel
	var readOut = new Uint8Array(4);
	gl.bindFramebuffer(gl.FRAMEBUFFER, this.frameBuffer);
	gl.readPixels(coords.x, coords.y, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, readOut);
	gl.bindFramebuffer(gl.FRAMEBUFFER, null);
	
	for (var i = 0; i < sceneObjects.length; i++)
	{
		if (this._compare(readOut, sceneObjects[i].uniqColor))
		{
			return i;
		}
	}
	
	return -1;
};