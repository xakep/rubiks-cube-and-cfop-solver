//
function BinarySearchFactory()
{
	return {
		// creates BinarySearch object.
		create: function() {
			return new BinarySearch();
		}
	}
}