// Entire cube.
function Cube(constantsService, spiralCreator, smallCubeInitializer)
{
	var cubes = []; // all cubes
	var layers = []; // layers (3 * cubeSize)
	var coloredStickers = 0;
	
	return {
		// Initialize the cube.
		// colors - array of 54 integers, can be null
		init: function(colors)
		{
			var cubeSize = constantsService.getCubeSize();
			
			// save 3 empty layers
			for (var i = 0; i < 3; i++)
			{
				var layer = [];
				for (var y = 0; y < cubeSize; y++)
				{
					layer.push([]);
				}
				layers.push(layer);
			}
			
			for (var i = 0; i < cubeSize; i++)
			{
				var cubesInSpiral = spiralCreator.create(i);

				for (var y = 0; y < cubesInSpiral.length; y++)
				{
					// cube has all 3 coordinates
					var smallCube = smallCubeInitializer.initializeSmallCube(cubesInSpiral[y], colors, coloredStickers);
					if (colors != null)
						coloredStickers += smallCube.getStickerSides().length;
					
					cubes.push(smallCube);
					
					for (var c = 0; c < 3; c++)
					{
						var value;
						var layerNumber; // rename it
						
						switch (c)
						{
							case 0:
								value = cubesInSpiral[y].slice(1);
								layerNumber = cubesInSpiral[y][0];
								break; // for X
								
							case 1:
								value = [ cubesInSpiral[y][2], cubesInSpiral[y][0] ];
								layerNumber = cubesInSpiral[y][1];
								break; // for Y
								
							case 2:
								value = cubesInSpiral[y].slice(0, 2);
								layerNumber = cubesInSpiral[y][2];
								break; // for Z
						}
						
						// position in the spiral
						var position = spiralCreator.getCubePositionInSpiral(value); // value has 2 integer

						layers[c][layerNumber][position] = cubes.length - 1;
					}
				};
			}
		},
		
		// function only for tests purposes
		// returns all amount of small cubes in the entire cube
		getCubesCount: function()
		{
			return cubes.length;
		},
		
		// Gets small cube by index
		getSmallCube: function(index)
		{
			return cubes[index];
		},
		
		// Gets the layer from specified plane by number
		getLayer: function(plane, number)
		{
			return layers[plane][number];
		}
	};
}