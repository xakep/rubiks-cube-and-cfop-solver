function PickedColors()
{
	var colors = null; // 26 cubes
	var usedColors = [9, 9, 9, 9, 9, 9]; // initially all colors are available

	const centers = [12, 16, 10, 14, 8, 25]; // order is important here
	const edges = [1, 3, 5, 7, 9, 11, 13, 15, 18, 20, 22, 24]; // not here
	const corners = [0, 2, 4, 6, 17, 19, 21, 23]; // and not here :)

	var _ctor = function() {
		colors = [];
		for (var i = 0; i < 26; i++)
		{
			colors.push([0, 0, 0]);
		}
	}();

	// returns true if cube already has the specified color on any side
	// this color should not be available
	var hasCubeSpecifiedColor = function(cubeNumber, color) {
		for (var i = 0; i < 3; i++)
		{
			if (colors[cubeNumber][i] == color)
				return true;
		}

		return false;
	};

	// checks other cubes for the same initialization
	var areThereTheSameCubes = function(cubeNumber, cellNumber, color, side) {
		if (cellNumber == 4) // check another centers
		{
			// check if opposite center is colored
			// find opposite cube number
			var position = centers.indexOf(cubeNumber);
			var oppositeCenter = position % 2 == 0
				? centers[position + 1]
				: centers[position - 1];

			// check is opposite center is colored
			var oppositeCenterColor = colors[oppositeCenter][Math.floor(position / 2)];
			if (oppositeCenterColor != 0)
			{
				var availableColor = oppositeCenterColor % 2 == 0 ? oppositeCenterColor - 1 : oppositeCenterColor + 1;
				if (oppositeCenterColor == color || color != availableColor)
					return true;
			}
			else // opposite center is NOT colored
			{
				// check if specified color is not used yet on other sides
				for (var i = 0; i < 6; i++)
				{
					if (centers[i] == cubeNumber || centers[i] == oppositeCenter)
						continue;

					var checkedSide = Math.floor(i / 2);
					var checkedColor = colors[ centers[i] ][ checkedSide ];

					if (checkedColor != 0)
					{
						var oppositeColor = checkedColor % 2 == 0 ? checkedColor - 1 : checkedColor + 1;
						if (color == checkedColor || color == oppositeColor)
							return true;
					}
				}
			}
		}

		if (cellNumber % 2 != 0) // edges
		{
			// can not contain opposite colors
			var oppositeColor = color % 2 == 0 ? color - 1 : color + 1;
			if (colors[cubeNumber].indexOf(oppositeColor) != -1)
				return true;

			// only 4 edges with same color
			var edgesCount = 0;
			for (var i = 0; i < edges.length; i++)
			{
				if (colors[ edges[i] ].indexOf(color) != -1)
				{
					edgesCount++;
					if (edgesCount == 4)
						return true; // no more fucking edges
				}
			}

			// check for the exactly same edges
			var checkedEdge = colors[cubeNumber].slice();
			checkedEdge[side] = color;

			if (checkedEdge.indexOf(0) == checkedEdge.lastIndexOf(0)) // edge has 2 colors
			{
				for (var i = 0; i < edges.length; i++) // check other edges
				{
					if (colors[ edges[i] ].indexOf(0) == colors[ edges[i] ].lastIndexOf(0)) // edge has 2 colors too
					{
						if (colors[ edges[i] ].indexOf(checkedEdge[0]) != -1 &&
							colors[ edges[i] ].indexOf(checkedEdge[1]) != -1 &&
							colors[ edges[i] ].indexOf(checkedEdge[2]) != -1)
							return true; // color is NOT available
					}
				}
			}
		}

		if (cellNumber % 2 == 0 && cellNumber != 4) // corners
		{
			// can not contain opposite colors
			var oppositeColor = color % 2 == 0 ? color - 1 : color + 1;
			if (colors[cubeNumber].indexOf(oppositeColor) != -1)
				return true;

			// only 4 corners with same color
			var cornersCount = 0;
			for (var i = 0; i < corners.length; i++)
			{
				if (colors[ corners[i] ].indexOf(color) != -1)
				{
					cornersCount++;
					if (cornersCount == 4)
						return true; // no more fucking corners
				}
			}

			// check for the exactly same corners
			var checkedCorner = colors[cubeNumber].slice();
			checkedCorner[side] = color;

			if (checkedCorner.indexOf(0) == -1) // corner has 3 colors
			{
				for (var i = 0; i < corners.length; i++) // check other corners
				{
					if (colors[ corners[i] ].indexOf(0) == -1) // corner has 3 colors too
					{
						if (colors[ corners[i] ].indexOf(checkedCorner[0]) != -1 &&
							colors[ corners[i] ].indexOf(checkedCorner[1]) != -1 &&
							colors[ corners[i] ].indexOf(checkedCorner[2]) != -1)
							return true; // color is NOT available
					}
				}
			}
		}

		return false;
	}

	return {
		// sets the color for specified cell
		// returns true if all cells are colored
		// false otherwise
		setCubeColor: function(cubeNumber, side, color) { // color might be 0 when cell is cleared
			// release the used color if needed
			var usedColor = colors[cubeNumber][side];
			if (usedColor != 0) // if cell is colored, release the color
				usedColors[usedColor - 1]++;

			if (colors[cubeNumber][side] != color)
			{
				if (color != 0) // if color is 0, cell is cleared
					usedColors[color - 1]--;
			}

			colors[cubeNumber][side] = color;

			for (var i = 0; i < 6; i++)
				if (usedColors[i] > 0) return false;

			return true;
		},

		getCubeColors: function(cubeNumber) {
			return colors[cubeNumber];
		},

		// returns available colors number in array
		// cellNumber is [0..8]
		getAvailableColors: function(cubeNumber, cellNumber, side) {
			var result = [];

			for (var i = 0; i < usedColors.length; i++)
			{
				// color is available if
				if (usedColors[i] > 0 && // there are free stickers
					!hasCubeSpecifiedColor(cubeNumber, i + 1) && // there are no same colors on the adjacent sides
					!areThereTheSameCubes(cubeNumber, cellNumber, i + 1, side)) // there are no same initialized cubes yet
					result.push(i); // color is available
			}

			return result;
		},
	}
}