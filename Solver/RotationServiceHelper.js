function RotationServiceHelper()
{
	var replaceColors = function(oldCube, newCube, plane) {
		var result = []; // 3 colors

		// save original signs
		for (var i = 0; i < 3; i++)
		{
			result.push(oldCube[i] < 0 ? -1 : 1);
		}
		
		result[plane] = Math.abs(newCube[plane]) * result[plane];
		
		switch (plane)
		{
			case 0:
				result[1] = Math.abs(newCube[2]) * result[1];
				result[2] = Math.abs(newCube[1]) * result[2];
				break;
			case 1:
				result[0] = Math.abs(newCube[2]) * result[0];
				result[2] = Math.abs(newCube[0]) * result[2];
				break;
			case 2:
				result[0] = Math.abs(newCube[1]) * result[0];
				result[1] = Math.abs(newCube[0]) * result[1];
				break;
		}
		
		return result;
	}
	
	return {
		// directions: 1 - CCW; 0 - CW
		// rotationPlane is X, Y or Z
		rotate: function(direction, colors, rotationPlane) {
			var result = [];
			
			if (direction == CCW) // CCW (see in negative direction)
			{
				for (var i = 0; i < 8; i++)
				{
					result.push(i + 2 > 7
						? replaceColors(colors[i], colors[i - 6], rotationPlane)
						: replaceColors(colors[i], colors[i + 2], rotationPlane));
				}
			}
			else // CW
			{
				for (var i = 0; i < 8; i++)
				{
					result.push(i < 2
						? replaceColors(colors[i], colors[i + 6], rotationPlane)
						: replaceColors(colors[i], colors[i - 2], rotationPlane));
				}
			}
			
			return result;
		}
	}
}