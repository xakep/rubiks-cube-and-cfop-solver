// generates random number for rotating layer
function RandomLayerService(constantsService, layerService)
{
	var currentRotatingLayer;
	var currentRotatingPlane;

	var previousLayers = []; // keeps 2 previous layers

	var checkLayer = function(layer)
	{
		if (previousLayers.length < 2)
			return true;

		if (previousLayers[0] != previousLayers[1])
			return true;

		if (previousLayers[0] != layer)
			return true;

		return false;
	}

	// generates new layer
	// prevents to use the same layer more than 2 times (270 rotation is crap)
	var generateRandom = function()
	{
		var newLayer = -1;

		while (newLayer == -1 || checkLayer(newLayer) != true)
		{
			newLayer = Math.floor(Math.random() * (constantsService.getCubeSize() * 3));
		}

		if (previousLayers.length > 1)
			previousLayers.shift();

		previousLayers.push(newLayer);

		return newLayer;
	}
	
	return {
		getCurrent: function() {
			return currentRotatingLayer;
		},
		
		rand: function() {
			currentRotatingLayer = generateRandom();
			var plane = layerService.getPlane(currentRotatingLayer);
			currentRotatingPlane = [0, 0, 0]; // X, Y, Z
			currentRotatingPlane[plane] = 1;
		},
		
		// not tested yet
		setLayer: function(number) {
			currentRotatingLayer = number;
			
			var plane = layerService.getPlane(currentRotatingLayer);
			currentRotatingPlane = [0, 0, 0]; // X, Y, Z
			currentRotatingPlane[plane] = 1;
		},
		
		getRotatingPlane: function() {
			return currentRotatingPlane;
		},

		getRandLayer: function()
		{
			return generateRandom();
		}
	}
}