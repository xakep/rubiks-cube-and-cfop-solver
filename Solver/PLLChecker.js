function PLLChecker(pllHelper)
{
	var _pllHelper = pllHelper;

	return {
		// checks whether specified cubeColors has a PLL
		checkPLL: function(cubeColors) {

			if (_pllHelper.isPLLSolved(cubeColors))
			{
				return [];
			}

			// Ua, Ub
			if (_pllHelper.is3x1OnTheLeft(cubeColors) &&
				_pllHelper.hasSideHeadlights(cubeColors, RIGHT_SIDE))
			{
				if (_pllHelper.hasHeadlightsOppositeColor(cubeColors, RIGHT_SIDE))
				{
					return _pllHelper.getPLLAlgorithm(1);
				}
				else
				{
					return _pllHelper.getPLLAlgorithm(0);
				}
			}

			// Ja
			if (_pllHelper.is3x1OnTheLeft(cubeColors) &&
				_pllHelper.hasSideInnerBlock(cubeColors, RIGHT_SIDE) &&
				!_pllHelper.hasOuterCornerOppositeColor(cubeColors, RIGHT_SIDE))
			{
				return _pllHelper.getPLLAlgorithm(14);
			}

			// Jb
			if (_pllHelper.is3x1OnTheLeft(cubeColors) &&
				_pllHelper.hasSideOuterBlock(cubeColors, RIGHT_SIDE) &&
				!_pllHelper.hasInnerCornerOppositeColor(cubeColors, RIGHT_SIDE))
			{
				return _pllHelper.getPLLAlgorithm(15);
			}

			// F
			if (_pllHelper.is3x1OnTheLeft(cubeColors) &&
				!_pllHelper.hasSideHeadlights(cubeColors, RIGHT_SIDE) &&
				!_pllHelper.hasSideInnerBlock(cubeColors, RIGHT_SIDE) &&
				!_pllHelper.hasSideOuterBlock(cubeColors, RIGHT_SIDE))
			{
				return _pllHelper.getPLLAlgorithm(7);
			}

			// Aa, Ab
			if (_pllHelper.hasSideInnerBlock(cubeColors, FRONT_SIDE) &&
				_pllHelper.hasSideInnerBlock(cubeColors, RIGHT_SIDE))
			{
				if (!_pllHelper.hasOuterCornerOppositeColor(cubeColors, FRONT_SIDE) &&
					_pllHelper.hasOuterCornerOppositeColor(cubeColors, RIGHT_SIDE))
				{
					return _pllHelper.getPLLAlgorithm(4);
				}
				if (_pllHelper.hasOuterCornerOppositeColor(cubeColors, FRONT_SIDE) &&
					!_pllHelper.hasOuterCornerOppositeColor(cubeColors, RIGHT_SIDE))
				{
					return _pllHelper.getPLLAlgorithm(5);
				}

				return _pllHelper.getPLLAlgorithm(19);
			}

			// H, Z
			if (_pllHelper.hasSideHeadlights(cubeColors, FRONT_SIDE) &&
				_pllHelper.hasSideHeadlights(cubeColors, RIGHT_SIDE))
			{
				if (_pllHelper.hasHeadlightsOppositeColor(cubeColors, FRONT_SIDE) &&
					_pllHelper.hasHeadlightsOppositeColor(cubeColors, RIGHT_SIDE))
				{
					return _pllHelper.getPLLAlgorithm(2);
				}

				if (!_pllHelper.hasHeadlightsOppositeColor(cubeColors, FRONT_SIDE) &&
					!_pllHelper.hasHeadlightsOppositeColor(cubeColors, RIGHT_SIDE))
				{
					if (_pllHelper.hasLeftSideCheckerPattern(cubeColors) &&
						_pllHelper.hasRightSideCheckerPattern(cubeColors))
					{
						return _pllHelper.getPLLAlgorithm(3);
					}
				}
			}

			// Y
			if (_pllHelper.hasSideOuterBlock(cubeColors, FRONT_SIDE) &&
				_pllHelper.hasSideOuterBlock(cubeColors, RIGHT_SIDE) &&
				_pllHelper.hasInnerCornerOppositeColor(cubeColors, FRONT_SIDE) &&
				_pllHelper.hasInnerCornerOppositeColor(cubeColors, RIGHT_SIDE))
			{
				return _pllHelper.getPLLAlgorithm(20);
			}

			// Na
			if (_pllHelper.hasSideInnerBlock(cubeColors, FRONT_SIDE) &&
				_pllHelper.hasOuterCornerOppositeColor(cubeColors, FRONT_SIDE) &&
				_pllHelper.hasSideOuterBlock(cubeColors, RIGHT_SIDE) &&
				_pllHelper.hasInnerCornerOppositeColor(cubeColors, RIGHT_SIDE))
			{
				return _pllHelper.getPLLAlgorithm(16);
			}

			// Nb
			if (_pllHelper.hasSideInnerBlock(cubeColors, RIGHT_SIDE) &&
				_pllHelper.hasOuterCornerOppositeColor(cubeColors, RIGHT_SIDE) &&
				_pllHelper.hasSideOuterBlock(cubeColors, FRONT_SIDE) &&
				_pllHelper.hasInnerCornerOppositeColor(cubeColors, FRONT_SIDE))
			{
				return _pllHelper.getPLLAlgorithm(17);
			}

			// T, Ra
			if (_pllHelper.hasSideHeadlights(cubeColors, FRONT_SIDE) &&
				_pllHelper.hasSideInnerBlock(cubeColors, RIGHT_SIDE))
			{
				if (_pllHelper.hasHeadlightsOppositeColor(cubeColors, FRONT_SIDE) &&
					!_pllHelper.hasOuterCornerOppositeColor(cubeColors, RIGHT_SIDE))
				{
					return _pllHelper.getPLLAlgorithm(18);
				}

				if (!_pllHelper.hasHeadlightsOppositeColor(cubeColors, FRONT_SIDE) &&
					!_pllHelper.hasOuterCornerOppositeColor(cubeColors, RIGHT_SIDE))
				{
					return _pllHelper.getPLLAlgorithm(12);
				}
			}

			// Rb, Gc
			if (_pllHelper.hasSideHeadlights(cubeColors, RIGHT_SIDE) &&
				!_pllHelper.hasHeadlightsOppositeColor(cubeColors, RIGHT_SIDE))
			{
				if (_pllHelper.hasSideInnerBlock(cubeColors, FRONT_SIDE) &&
					!_pllHelper.hasOuterCornerOppositeColor(cubeColors, FRONT_SIDE))
				{
					return _pllHelper.getPLLAlgorithm(13);
				}

				if (_pllHelper.hasSideOuterBlock(cubeColors, FRONT_SIDE) &&
					!_pllHelper.hasInnerCornerOppositeColor(cubeColors, FRONT_SIDE) &&
					!_pllHelper.hasRightSideCheckerPattern(cubeColors))
				{
					return _pllHelper.getPLLAlgorithm(10);
				}
			}

			// Ga
			if (_pllHelper.hasSideHeadlights(cubeColors, FRONT_SIDE) &&
				!_pllHelper.hasHeadlightsOppositeColor(cubeColors, FRONT_SIDE) &&
				_pllHelper.hasSideOuterBlock(cubeColors, RIGHT_SIDE) &&
				!_pllHelper.hasInnerCornerOppositeColor(cubeColors, RIGHT_SIDE) &&
				!_pllHelper.hasLeftSideCheckerPattern(cubeColors))
			{
				return _pllHelper.getPLLAlgorithm(8);
			}

			// Gb
			if (_pllHelper.hasSideInnerBlock(cubeColors, FRONT_SIDE) &&
				_pllHelper.hasOuterCornerOppositeColor(cubeColors, FRONT_SIDE) &&
				!_pllHelper.hasSideHeadlights(cubeColors, RIGHT_SIDE) &&
				!_pllHelper.hasSideInnerBlock(cubeColors, RIGHT_SIDE) &&
				!_pllHelper.hasSideOuterBlock(cubeColors, RIGHT_SIDE) &&
				_pllHelper.haveOuterCornersSameColor(cubeColors))
			{
				return _pllHelper.getPLLAlgorithm(9);
			}

			// Gd
			if (_pllHelper.hasSideInnerBlock(cubeColors, RIGHT_SIDE) &&
				_pllHelper.hasOuterCornerOppositeColor(cubeColors, RIGHT_SIDE) &&
				!_pllHelper.hasSideHeadlights(cubeColors, FRONT_SIDE) &&
				!_pllHelper.hasSideInnerBlock(cubeColors, FRONT_SIDE) &&
				!_pllHelper.hasSideOuterBlock(cubeColors, FRONT_SIDE) &&
				_pllHelper.haveOuterCornersSameColor(cubeColors))
			{
				return _pllHelper.getPLLAlgorithm(11);
			}

			// E
			if (!_pllHelper.hasSideHeadlights(cubeColors, RIGHT_SIDE) &&
				!_pllHelper.hasSideInnerBlock(cubeColors, RIGHT_SIDE) &&
				!_pllHelper.hasSideOuterBlock(cubeColors, RIGHT_SIDE) &&

				!_pllHelper.hasSideHeadlights(cubeColors, FRONT_SIDE) &&
				!_pllHelper.hasSideInnerBlock(cubeColors, FRONT_SIDE) &&
				!_pllHelper.hasSideOuterBlock(cubeColors, FRONT_SIDE) &&

				!_pllHelper.isThereCheckerPattern(cubeColors) &&

				_pllHelper.haveLeftOuterCornerAndRightEdgeSameColor(cubeColors))
			{
				return _pllHelper.getPLLAlgorithm(6);
			}

			return -1;
		},

		/*
		
		Track list
		
		1. Okzide - Miles Away (Melodic Dubstep)
		2. Kasbo - Horizon
		3. Grafit - Be Free
		4. Vicetone - California (Radio Edit)
		5. The Unamazing Brit - Feel The Beat
		6. Dennis Hidden & B One - Legacy
		7. Wolfgang Gartner - Love & War (Original Mix)
		8. MitiS - Innocent Discretion
		9. N'to - Trauma (Worakls Remix)
		
		and
	
		http://www.audiomicro.com/tracks/dialog/513441

		*/
	}
}