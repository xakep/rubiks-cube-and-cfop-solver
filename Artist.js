function Artist(glBuffersFacade, randomLayerService)
{
	var mvMatrix = null;
	var pMatrix = null;
	
	var mvMatrixStack = [];
	
	var sceneRotatingXAngle = 30.0;
	var sceneRotatingYAngle = -25.0;
	var distance = -10.0;

	var picker;
	
	// private
	
	var mvPushMatrix = function()
	{
		var copy = mat4.create();
		mat4.set(mvMatrix, copy);
		mvMatrixStack.push(copy);
	}
	
	var mvPopMatrix = function()
	{
		if (mvMatrixStack.length == 0)
		{
			throw "Invalid popMatrix!";
		}
		
		mvMatrix = mvMatrixStack.pop();
	}
	
	var degToRad = function(degrees)
	{
		return degrees * Math.PI / 180;
	}
	
	var setMatrixUniforms = function(pickingColor)
	{
		gl.uniformMatrix4fv(prg.pMatrixUniform, false, pMatrix);
		gl.uniformMatrix4fv(prg.mvMatrixUniform, false, mvMatrix);
		if (pickingColor)
		{
			gl.uniform4fv(prg.uPickingColor, pickingColor);
		}
		
		var normalMatrix = mat3.create();
		mat4.toInverseMat3(mvMatrix, normalMatrix);
		mat3.transpose(normalMatrix);
		gl.uniformMatrix3fv(prg.nMatrixUniform, false, normalMatrix);
	}

	// common mode
	// draws 2 buffers: static and rotating parts
	var draw2buffers = function(angle)
	{
		mvPushMatrix();
			
		// static vertices
		gl.bindBuffer(gl.ARRAY_BUFFER, glBuffersFacade.getBuffer(1001));
		gl.vertexAttribPointer(prg.vertexPosition, 3, gl.FLOAT, false, 0, 0);
		
		// static normals
		gl.bindBuffer(gl.ARRAY_BUFFER, glBuffersFacade.getBuffer(1007));
		gl.vertexAttribPointer(prg.vertexNormal, 3, gl.FLOAT, false, 0, 0);
		
		// static colors
		gl.bindBuffer(gl.ARRAY_BUFFER, glBuffersFacade.getBuffer(1003));
		gl.vertexAttribPointer(prg.vertexColor, 4, gl.FLOAT, false, 0, 0);
		
		// static indices
		var staticIndicesBuffer = glBuffersFacade.getBuffer(1005);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, staticIndicesBuffer);
		
		setMatrixUniforms();
		
		// draw static part
		// numItems is small cubes count * count of triangles per one small cube
		// 3 is count of floats per vertice
		gl.drawElements(gl.TRIANGLES, 3 * staticIndicesBuffer.numItems, gl.UNSIGNED_SHORT, 0);
		
		mvPopMatrix();
		
		// rotating part
		mvPushMatrix();

		mat4.rotate(mvMatrix, degToRad(angle), randomLayerService.getRotatingPlane());
		
		// rotating vertices
		gl.bindBuffer(gl.ARRAY_BUFFER, glBuffersFacade.getBuffer(1000));
		gl.vertexAttribPointer(prg.vertexPosition, 3, gl.FLOAT, false, 0, 0);
		
		// rotating normals
		gl.bindBuffer(gl.ARRAY_BUFFER, glBuffersFacade.getBuffer(1006));
		gl.vertexAttribPointer(prg.vertexNormal, 3, gl.FLOAT, false, 0, 0);
		
		// rotating colors
		gl.bindBuffer(gl.ARRAY_BUFFER, glBuffersFacade.getBuffer(1002));
		gl.vertexAttribPointer(prg.vertexColor, 4, gl.FLOAT, false, 0, 0);
		
		// rotating indices
		var rotatingIndicesBuffer = glBuffersFacade.getBuffer(1004);
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, rotatingIndicesBuffer);
		
		setMatrixUniforms();
		
		// draw rotating part
		gl.drawElements(gl.TRIANGLES, 3 * rotatingIndicesBuffer.numItems, gl.UNSIGNED_SHORT, 0);
	
		mvPopMatrix();
	}

	// interactive mode
	// draws 55 buffers: frame and 54 stickers
	var draw55buffers = function()
	{
		var sceneObjects = glBuffersFacade.getFrameAndStickers();

		for (var i = 0; i < sceneObjects.length; i++)
		{
			mvPushMatrix();
				
			// vertices
			gl.bindBuffer(gl.ARRAY_BUFFER, sceneObjects[i].vbo);
			gl.vertexAttribPointer(prg.vertexPosition, 3, gl.FLOAT, false, 0, 0);
			
			// normals
			gl.bindBuffer(gl.ARRAY_BUFFER, sceneObjects[i].nbo);
			gl.vertexAttribPointer(prg.vertexNormal, 3, gl.FLOAT, false, 0, 0);
			
			// colors
			gl.bindBuffer(gl.ARRAY_BUFFER, sceneObjects[i].cbo);
			gl.vertexAttribPointer(prg.vertexColor, 4, gl.FLOAT, false, 0, 0);
			
			// indices
			var indicesBuffer = sceneObjects[i].ibo;
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indicesBuffer);
			
			setMatrixUniforms(sceneObjects[i].uniqColor);
			
			gl.drawElements(gl.TRIANGLES, 3 * indicesBuffer.numItems, gl.UNSIGNED_SHORT, 0);
			
			mvPopMatrix();
		}
	}
	
	// public

	return {
		init: function(_picker) {
			mvMatrix = mat4.create();
			pMatrix = mat4.create();

			if (_picker != null)
			{
				picker = _picker;
			}
		},
		
		increaseSceneAngle: function() {
			sceneRotatingXAngle += 5.0;
		},
		
		changeSceneYAngle: function(direction) {
			sceneRotatingYAngle += 15.0 * direction;
		},
		
		changeSceneXAngle: function(direction) {
			sceneRotatingXAngle += 15.0 * direction;
		},

		zoom: function(delta) {
			distance += delta;
		},
		
		drawScene: function(angle) {
			gl.clearColor(0.0, 0.0, 0.0, 1.0);
			gl.enable(gl.DEPTH_TEST);
			gl.enable(gl.CULL_FACE);
			gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
			
			gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
			mat4.perspective(45, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0, pMatrix);
			
			mat4.identity(mvMatrix);
			mat4.translate(mvMatrix, [0.0, 0.0, distance]);

			mat4.rotate(mvMatrix, degToRad(sceneRotatingXAngle), [1, 0, 0]);
			mat4.rotate(mvMatrix, degToRad(sceneRotatingYAngle), [0, 1, 0]);
			
			if (angle != null) // normal renderer
			{
				draw2buffers(angle);
			}
			else // interactive
			{
				// off-screen rendering
				gl.bindFramebuffer(gl.FRAMEBUFFER, picker.frameBuffer);
				gl.uniform1i(prg.uOffScreen, true);
				draw55buffers();
				
				// on-screen rendering
				gl.bindFramebuffer(gl.FRAMEBUFFER, null);
				gl.uniform1i(prg.uOffScreen, false);
				draw55buffers();
			}
		}
	}
}