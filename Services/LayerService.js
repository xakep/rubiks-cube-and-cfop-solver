function LayerService(constantsService)
{
	return {
		// X, Y or Z
		// layer is current rotating layer
		getPlane: function(layer) {
			return Math.floor(layer / constantsService.getCubeSize());
		},
		
		// [0 .. cubeSize - 1]
		getNumber: function(layer) {
			return Math.floor(layer % constantsService.getCubeSize());
		}
	}
}