function F2LSlotSolver(F2LHelper, rotationService, mappingService)
{
	// dependencies
	var _f2lHelper = F2LHelper;
	var _rotationService = rotationService;
	var _mappingService = mappingService;

	var formatMoves = function(moves) {
		var res = "";
		for (var i = 0; i < moves.length; i++)
			res += "[" + moves[i].length + "][" + moves[i] + "], ";
		return res;
	}

	return {
		// rotates the specified cube 4 times
		// looks for the slot that should be closed first
		// returns moves to close one of the opened slots
		closeSlot: function(cubeColors) {

			var moves = []; // moves to close every slot
			var minMoves = -1;

			var rotatedCubeColors = null;

			for (var i = 0; i < 4; i++)
			{
				var isSlotClosed = _f2lHelper.isSlotClosed(i == 0 ? cubeColors : rotatedCubeColors);

				if (isSlotClosed == false)
				{
					// slot is opened

					// check whether the corner is in place
					var movesToPlaceCorner = _f2lHelper.getMovesToPlaceCorner(i == 0 ? cubeColors : rotatedCubeColors); // can be []
					var a; // temp array for moves

					if (movesToPlaceCorner.length == 0)
					{
						a = _f2lHelper.findAlgorithm(i == 0 ? cubeColors : rotatedCubeColors);
					}
					else
					{
						// place the corner
						var temp = _rotationService.applyMoves(i == 0 ? cubeColors : rotatedCubeColors, movesToPlaceCorner);

						// find the algorithm
						a = movesToPlaceCorner.concat( _f2lHelper.findAlgorithm(temp) ); // concat with corner moves
					}

					moves.push( a );


					/*
					// check if found algorithm really closes the slot (for test purposes only)
					var currentClosedSlots = this.getClosedSlotsCount(i == 0 ? cubeColors : rotatedCubeColors);
					// apply found algorithm
					var cubeWithClosedSlot = _rotationService.applyMoves(
						i == 0 ? cubeColors : rotatedCubeColors, a);
					var newClosedSlots = this.getClosedSlotsCount(cubeWithClosedSlot);
					
					if (currentClosedSlots != newClosedSlots - 1)
					{
						console.warn("Was closed slots: " + currentClosedSlots + ", now: " + newClosedSlots);
						if (currentClosedSlots == newClosedSlots) // where was NOT closed any slots
							console.error("i = " + i + ": ERROR: found algorithm didn't close the slot! " + a + "\n");
					}
					*/

					// findAlgorithm can't return [] cause if slot is closed
					// this code won't be execute, so
					if (minMoves == -1 || a.length < moves[minMoves].length)
						minMoves = i;
				}
				else // slot is closed
				{
					moves.push([]);
				}

				// should I do that on last iteration? probably NOT, cause model is immutable
				rotatedCubeColors = _rotationService.rotateEntireCube(i == 0 ? cubeColors : rotatedCubeColors);
			}

			// return min moves
			if (minMoves == -1)
			{
				console.error("SLOT SOLVER ERROR, MIN MOVES = -1, MOVES ARE: " + moves);
				return [];
			}

			return minMoves == 0
				? moves[minMoves]
				: _mappingService.mappedMoves( moves[minMoves], minMoves );
		},

		getClosedSlotsCount: function(cubeColors) {
			var closedSlots = 0;

			var bottomColor = cubeColors.getSideColor(Y, M10);
			var frontColor = cubeColors.getSideColor(Z, F11);
			var rightColor = cubeColors.getSideColor(X, M21);
			var backColor = cubeColors.getSideColor(Z, B11);
			var leftColor = cubeColors.getSideColor(X, M01);

			if (cubeColors.getSideColor(Z, F21) == frontColor &&
				cubeColors.getSideColor(X, F21) == rightColor &&
				cubeColors.getSideColor(Y, F20) == bottomColor &&
				cubeColors.getSideColor(Z, F20) == frontColor &&
				cubeColors.getSideColor(X, F20) == rightColor)
				closedSlots++;

			if (cubeColors.getSideColor(Z, B21) == backColor &&
				cubeColors.getSideColor(X, B21) == rightColor &&
				cubeColors.getSideColor(Y, B20) == bottomColor &&
				cubeColors.getSideColor(Z, B20) == backColor &&
				cubeColors.getSideColor(X, B20) == rightColor)
				closedSlots++;

			if (cubeColors.getSideColor(Z, B01) == backColor &&
				cubeColors.getSideColor(X, B01) == leftColor &&
				cubeColors.getSideColor(Y, B00) == bottomColor &&
				cubeColors.getSideColor(Z, B00) == backColor &&
				cubeColors.getSideColor(X, B00) == leftColor)
				closedSlots++;

			if (cubeColors.getSideColor(Z, F01) == frontColor &&
				cubeColors.getSideColor(X, F01) == leftColor &&
				cubeColors.getSideColor(Y, F00) == bottomColor &&
				cubeColors.getSideColor(Z, F00) == frontColor &&
				cubeColors.getSideColor(X, F00) == leftColor)
				closedSlots++;

			return closedSlots;
		}
	}
}