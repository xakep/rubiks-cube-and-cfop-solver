onmessage = function(message)
{
	var allColors = message.data[0];
	var grayColor = message.data[1];
	var cubeSize = message.data[2];

	var additionalColors = []; // for one cube
	// 12 jumpers, 4 verts each = 48 verts
	// + 8 corners, 6 verts each = 48 verts
	// -> 96 verts
	if (cubeSize < 5)
	{
		for (var y = 0; y < 96; y++)
		{
			additionalColors = additionalColors.concat(grayColor);
		}
	}

	// build static buffer
	var staticColors = [];
	var smallStaticCubes = message.data[4];

	for (var i = 0; i < smallStaticCubes.length; i++)
	{
		staticColors = staticColors
			.concat(allColors[smallStaticCubes[i]])
			.concat(additionalColors);
	}

	// build rotating buffer
	var rotatingColors = [];
	var smallCubes = message.data[3];

	for (var i = 0; i < smallCubes.length; i++)
	{
		rotatingColors = rotatingColors
			.concat(allColors[ smallCubes[i]])
			.concat(additionalColors);
	}

	postMessage([message.data[5], staticColors, rotatingColors]);
}