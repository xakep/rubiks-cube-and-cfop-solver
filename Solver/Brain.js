function Brain(colorsChangeService, rotationService, solver)
{
	var _colorsChangeService = colorsChangeService;
	var _rotationService = rotationService;
	var _solver = solver;

	return {
		findShortestSolution: function() {
			var cubeColors = _colorsChangeService.getCubeColors(); // initial cube
			var solutions = [];

			// find sol for bottom
			solutions.push( _solver.solve(cubeColors));

			for (var i = 1; i < 6; i++)
			{
				var rotated = _rotationService.rotateEntireCube(cubeColors, i);
				solutions.push( _solver.solve(rotated) );
			}

			var minMoves = 0;
			for (var i = 1; i < solutions.length; i++)
			{
				if (solutions[i].length < solutions[minMoves].length)
				{
					minMoves = i;
				}
			}

			console.log(solutions[minMoves].length);

			return [minMoves, solutions[minMoves]];
		},
	}
}