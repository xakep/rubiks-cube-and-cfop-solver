function Renderer()
{
	var randomLayerService;
	var glBuffersFacade;
	var colorsChangeService;
	var constantsService;
	var layerService;
	var asyncGenerationService;
	
	var cube = null;
	
	var artist = null;
	var rotatingLayerAngle = 0.0;

	// other vars
	var direction = 1; // layer rotation direction
	var moves = []; // moves to apply for solve the cube
	var rotateEntireCube = 0; // a flag indicating whether all layers should be rotated simultaneously
	var isNextLayerGenerated = -1;
	var cubeDesiredPosition = null; // solver uses this

	// states
	var state;
	var STATE_IDLE = 0;
	var STATE_SHUFFLING = 1;
	var STATE_ROTATEENTIRECUBE = 2;
	var STATE_SETCOLORS = 3; // in this state 'space' & 'm' don't work
	var STATE_SOLVING = 4;
	var STATE_LASTROTATION = 5;

	var renderLoop = function() {
		if (artist != null) // artist is null when renderer is released
		{
			utils.requestAnimFrame(renderLoop);

			if (cubeDesiredPosition != null) // for solver
			{
				var sign = cubeDesiredPosition.getDegree() > 0 ? -1 : 1;
				cubeDesiredPosition.getAxis() == 0 // X
					? artist.changeSceneXAngle(sign)
					: artist.changeSceneYAngle(sign);

				var degree = cubeDesiredPosition.updateDegree(sign);
				if (degree == 0)
					cubeDesiredPosition = null; // stop
			}

			var size = constantsService.getCubeSize(); // cubeSize

			switch (state)
			{
				case STATE_SHUFFLING:
					
					if (isNextLayerGenerated == -1)
					{
						isNextLayerGenerated = randomLayerService.getRandLayer();

						colorsChangeService.updateColors(
							direction == 1 ? 1 : 0,
							Math.floor(randomLayerService.getCurrent() / size),
							cube.getLayer(
								Math.floor(randomLayerService.getCurrent() / size),
								Math.floor(randomLayerService.getCurrent() % size)));

						asyncGenerationService.generate(isNextLayerGenerated);
					}

					break;

				case STATE_ROTATEENTIRECUBE:

					if (rotatingLayerAngle == 0.0)
					{
						var move = moves.shift();
						if (move < 0)
						{
							direction = -1; // change direction
						}

						var layer = Math.abs(move) - 1;

						for (var i = 0; i < size; i++)
						{
							colorsChangeService.updateColors(
								direction == 1 ? 1 : 0,
								Math.floor(layer / size),
								cube.getLayer(
									Math.floor(layer / size),
									Math.floor(layer % size) + i));
						}

						if (rotateEntireCube == 1) // last one
						{
							isNextLayerGenerated = moves.length > 0
								? Math.abs(moves[0]) - 1
								: randomLayerService.getRandLayer();
							asyncGenerationService.generate(isNextLayerGenerated);
						}
						else
						{
							asyncGenerationService.generate(layer);
						}
					}

					break;

				case STATE_SOLVING:

					if (rotatingLayerAngle == 0.0)
					{
						var move = moves.shift(); // I already have buffers for this move
						if (move < 0)
						{
							direction = -1; // change direction
						}

						if (moves.length > 0)
						{
							colorsChangeService.updateColors(
								direction == 1 ? 1 : 0,
								Math.floor(randomLayerService.getCurrent() / size),
								cube.getLayer(
									Math.floor(randomLayerService.getCurrent() / size),
									Math.floor(randomLayerService.getCurrent() % size)));

							move = moves[0]; // next move
							asyncGenerationService.generate(Math.abs(move) - 1);
						}
						else // update buffers last time
						{
							colorsChangeService.updateColors(
								direction == 1 ? 1 : 0,
								Math.floor(randomLayerService.getCurrent() / size),
								cube.getLayer(
									Math.floor(randomLayerService.getCurrent() / size),
									Math.floor(randomLayerService.getCurrent() % size)));

							asyncGenerationService.generate(Math.abs(move) - 1);
						}
					}

					break;
			}

			// Rotate
			if (state != STATE_IDLE && state != STATE_SETCOLORS)
			{
				var speed = rotateEntireCube > 0 ? 5.0 : 6.0;
				rotatingLayerAngle += speed * direction;
			}

			if (rotatingLayerAngle == 90.0 * direction) // rotation of layer is done
			{
				rotatingLayerAngle = 0.0; // set angle to 0
				direction = 1;

				switch (state)
				{
					case STATE_SHUFFLING:

						randomLayerService.setLayer(isNextLayerGenerated);
						glBuffersFacade.generate(randomLayerService.getCurrent(), false, true);
						isNextLayerGenerated = -1;

						break;

					case STATE_LASTROTATION:

						randomLayerService.setLayer(isNextLayerGenerated);
						glBuffersFacade.generate(randomLayerService.getCurrent(), false, true);
						isNextLayerGenerated = -1;
						
						state = STATE_IDLE;

						break;

					case STATE_ROTATEENTIRECUBE:

						if (rotateEntireCube > 0)
							rotateEntireCube--

						if (rotateEntireCube > 0)
						{
							// one more rotation, same layer = 1
							glBuffersFacade.generate(randomLayerService.getCurrent(), true, true);
						}
						else
						{
							glBuffersFacade.generate(isNextLayerGenerated, false, true);
							randomLayerService.setLayer(isNextLayerGenerated);
							isNextLayerGenerated = -1;
							
							state = moves.length == 0
								? STATE_IDLE
								: STATE_SOLVING;
						}

						break;

					case STATE_SOLVING:

						if (moves.length > 0)
						{
							randomLayerService.setLayer(Math.abs(moves[0]) - 1);
							glBuffersFacade.generate(Math.abs(moves[0]) - 1, false, true);
						}
						else
						{
							glBuffersFacade.generate(randomLayerService.getCurrent(), false, true);
							state = STATE_IDLE;
						}

						break;
				}
			}
			
			// draw
			artist.drawScene(rotatingLayerAngle);
		}
	}
	
	return {
		run: function(_glBuffersFacade, _randomLayerService, _cube,
			_constantsService, _colorsChangeService, _asyncGenerationService)
		{
			cube = _cube;
			constantsService = _constantsService;
			colorsChangeService = _colorsChangeService;
			randomLayerService = _randomLayerService;
			asyncGenerationService = _asyncGenerationService;
			glBuffersFacade = _glBuffersFacade;

			randomLayerService.rand();
		
			glBuffersFacade.generate(randomLayerService.getCurrent());
			
			artist = new Artist(glBuffersFacade, randomLayerService);
			artist.init();
			state = STATE_IDLE;
			
			renderLoop();
		},
		
		shuffleTheCube: function() {
			if (state != STATE_SETCOLORS && (state == STATE_IDLE || state == STATE_SHUFFLING))
			{
				state = state == STATE_SHUFFLING
					? rotatingLayerAngle != 0.0 ? STATE_LASTROTATION : STATE_IDLE
					: STATE_SHUFFLING;
			}
		},
		
		// rotates the cube around X, step is 5.0 ('a' key)
		rotateCube: function() {
			artist.increaseSceneAngle();
		},

		// position's type is CubeDesiredPosition
		rotateCubeToPosition: function(position) {
			cubeDesiredPosition = position;
		},

		// this is for solver
		// changes the bottom layer rotating all layers
		// desiredSides are: 0 - Bottom, 1 - Front, 2 - Right, 3 - Back, 4 - Left, 5 - Top
		// solution is an array
		// solution[0] is a desiredSide
		// solution[1] are moves to perform
		solveCube: function(solution) {
			// specify first layer in the plane (1 - X, 1 + _cubeSize - Y, 1 + 2 * _cubeSize - Z)
			if (constantsService.getCubeSize() == 3 && (state == STATE_IDLE || state == STATE_SETCOLORS))
			{
				var sides = [ [], [1], [-7], [-1], [7], [1, 1] ]; // for 3x3 cube
				rotateEntireCube = sides[ solution[0] ].length;
				moves = sides[ solution[0] ].concat(solution[1]);

				if (rotateEntireCube > 0)
				{
					var move = moves[0]; // if rotateEntireCube > 0 moves.len also > 0
					randomLayerService.setLayer(Math.abs(move) - 1);

					glBuffersFacade.generate(randomLayerService.getCurrent(), true); // generate entire cube synchronously

					state = STATE_ROTATEENTIRECUBE;
				}
				else
				{
					if (moves.length > 0)
					{
						var move = moves[0];
						randomLayerService.setLayer(Math.abs(move) - 1);

						glBuffersFacade.generate(randomLayerService.getCurrent(), false); // generate entire cube asynchronously

						state = STATE_SOLVING;
					}
					else
					{
						state = STATE_IDLE;
					}
				}
			}
		},
		
		// performs specified moves
		applyMoves: function(movesToPerform) {
			if (movesToPerform.length > 0)
			{
				moves = movesToPerform;

				var move = moves[0];
				randomLayerService.setLayer(Math.abs(move) - 1);

				glBuffersFacade.generate(randomLayerService.getCurrent(), false);

				state = STATE_SOLVING;
			}
		},

		// returns current state (used in jQueryLogic)
		getState: function()
		{
			return state;
		},

		// zooms the view, +1 - in; -1 - out
		zoom: function(delta) {
			artist.zoom(delta);
		},
		
		// this method used by solver
		// when user sets the cube colors
		// PROBABLY UNUSED
		setColor: function(cubeNumber, side, color) {
			colorsChangeService.setSideColor(cubeNumber, side, color);
			glBuffersFacade.generate(randomLayerService.getCurrent());
		},

		// stops animations
		release: function() {
			state = STATE_IDLE;
			artist = null;
		},
		
		// before solving, discolors entire cube
		// TODO: remove it, unused
		discolorEntireCube: function() {
			state = STATE_SETCOLORS;
			rotatingLayerAngle = 0.0;

			for (var y = 0; y < 6; y++) // 6 layers
			{
				var layer = cube.getLayer(Math.floor(y / 2), Math.floor(y % 2) == 0 ? 0 : 2);

				for (var i = 0; i < layer.length; i++)
				{
					colorsChangeService.setSideColor(layer[i], Math.floor(y / 2), 7);
				}
			}
			
			glBuffersFacade.generate(randomLayerService.getCurrent());
		},

		getCanvasCoordinates: function() {
			// not implemented (unused)	
		}
	}
}