// if colors is specified, initialize cube with it
function Initializer(colors)
{
	var constantsService;
	var cubeCoordinatesLoader;
	var smallCubeFactory;
	var smallCubeColorsService;
	var colorsChangeService;
	var smallCubeInitializer;
	var binarySearchFactory;
	var spiralCreator;
	var glBuffersFactory;
	var layerService;
	var randomLayerService;
	var jsBuffersFactory;
	var asyncGeneratorWorker;
	var glBuffersFacade;
	var asyncGenerationService;
	var cube;

	var renderer = null;

	var modelLoaded = function() {
		smallCubeFactory = new SmallCubeFactory(cubeCoordinatesLoader);
		smallCubeColorsService = new SmallCubeColorsService(constantsService);
		colorsChangeService = new ColorsChangeService(constantsService, smallCubeColorsService, new CubeColorsFactory());
		smallCubeInitializer = new SmallCubeInitializer(
			smallCubeFactory,
			constantsService,
			smallCubeColorsService,
			colorsChangeService);
		binarySearchFactory = new BinarySearchFactory();
		spiralCreator = new SpiralCreator(constantsService, binarySearchFactory);
		glBuffersFactory = new GLBuffersFactory();
		layerService = new LayerService(constantsService);
		randomLayerService = new RandomLayerService(constantsService, layerService);

		cube = new Cube(constantsService, spiralCreator, smallCubeInitializer);
		cube.init(colors);

		jsBuffersFactory = new JSBuffersFactory(
			constantsService,
			layerService,
			cubeCoordinatesLoader,
			cube,
			colorsChangeService);

		glBuffersFacade = new GLBuffersFacade(jsBuffersFactory, glBuffersFactory);

		asyncGeneratorWorker = new Worker("AsyncBuffersGenerator.js");

		asyncGenerationService = new AsyncGenerationService(
			constantsService,
			asyncGeneratorWorker,
			colorsChangeService,
			layerService,
			cube,
			glBuffersFacade);

		renderer.run(glBuffersFacade, randomLayerService, cube,
			constantsService, colorsChangeService, asyncGenerationService);

		if (colors != null)
		{
			var solution = findSolution();

			movesToSolve = solution[0][1];
			position = 0;

			$('#solutionMovesCount').text(solution[1]);
			renderer.solveCube([solution[0][0], []]);
		}
	}

	var findSolution = function() {
		if (constantsService.getCubeSize() == 3)
		{
			var rotationService = new RotationService(
				cube,
				new RotationServiceHelper(),
				new CubeColorsFactory(),
				layerService);
				
			var mappingService = new MappingService();
				
			var crossSolver = new CrossSolver(
				rotationService,
				new CrossSolverHelper(),
				mappingService);
				
			var f2l = new F2L(
				rotationService,
				new F2LSlotSolver(new F2LHelper(), rotationService, mappingService));
					
			var oll = new OLL(
				new OLLHelper(),
				new BinarySearchFactory(),
				rotationService,
				mappingService);
			
			var pllChecker = new PLLChecker(new PLLHelper(constantsService));
				
			var pll = new PLL(
				pllChecker,
				rotationService,
				mappingService);
				
			var solver = new Solver(
				crossSolver,
				f2l,
				oll,
				pll,
				new TopLayerOrientator(constantsService),
				new Optimizer());

			var brain = new Brain(colorsChangeService, rotationService, solver);
			var solution = brain.findShortestSolution(); // [movesLen, moves]
			
			// returns solution and moves count
			return [solution, solver.calculateSolutionMovesCount(solution[1])];
		}
	}

	return {
		init: function(cubeSize, interactive) {
			constantsService = new ConstantsService();
			constantsService.setCubeSize(cubeSize);

			cubeCoordinatesLoader = new CubeCoordinatesLoader(modelLoaded);
			cubeCoordinatesLoader.load(cubeSize < constantsService.getDetailedCubeLimit()
					? "SmallCubeCoordsDetailed.json" // cube with more polygons, 92 triangles
					: "SmallCubeCoords.json"); // simple, 12 triangles per cube

			renderer = interactive
				? new InteractiveRenderer()
				: new Renderer();
			return renderer;
		},

		findSolution: function() {
			return findSolution();	
		}
	}
}