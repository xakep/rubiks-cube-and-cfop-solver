function CubeColorsFactory()
{
	return {
		// creates object which contains colors for all 26 small cubes
		// colors is array of 26 arrays
		// returns CubeColors object
		createCubeColors: function(colors) {
			var cubeColors = new CubeColors();
			cubeColors.init(colors);
			
			return cubeColors;
		}
	}
}