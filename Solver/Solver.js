function Solver(crossSolver, f2l, oll, pll, topLayerOrientator, optimizer)
{
	var _crossSolver = crossSolver;
	var _f2l = f2l;
	var _oll = oll;
	var _pll = pll;
	var _topLayerOrientator = topLayerOrientator;
	var _optimizer = optimizer;
	
	return {
		// should return moves required to solve the cube
		solve: function(cubeColors) {
			var orientationMoves = _crossSolver.orientate(cubeColors);
			var crossMoves = _crossSolver.solveCross(cubeColors, orientationMoves);
			var f2lMoves = _f2l.solveF2L(crossMoves.getCubeColors());
			var ollMoves = _oll.solveOLL(f2lMoves.getCubeColors());
			var pllMoves = _pll.solvePLL(ollMoves.getCubeColors());
			
			var result = orientationMoves
				.concat(crossMoves.getMoves())
				.concat(f2lMoves.getMoves())
				.concat(ollMoves.getMoves())
				.concat(pllMoves.getMoves())
				.concat(_topLayerOrientator.orientate(pllMoves.getCubeColors()));
			
			return _optimizer.optimize(result);
		},

		// returns solution moves count (180 rotation is one move)
		calculateSolutionMovesCount: function(moves) {
			var doubled = 0;
			for (var i = 1; i < moves.length; i++)
			{
				if (moves[i] == moves[i - 1])
				{
					doubled++;
					i++;
				}
			}
			
			return moves.length - doubled;
		}
	}
}