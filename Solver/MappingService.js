function MappingService()
{
	// map for -X, -Z, +X
	var map = [
		[7, 8, 9, 4, 5, 6, -3, -2, -1], // -X
		[-3, -2, -1, 4, 5, 6, -9, -8, -7], // -Z
		[-9, -8, -7, 4, 5, 6, 1, 2, 3], // +X
	];
	
	return {
		// maps the specified moves according to specified side
		// sides are 1 (-X), 2 (-Z), 3 (+X).
		mappedMoves: function(moves, side) {
			var result = [];
			for (var i = 0; i < moves.length; i++)
			{
				if (Math.abs(moves[i]) > 3 && Math.abs(moves[i]) < 7) // skip Y
				{
					result.push(moves[i]);
					continue;
				}
				
				// mapping
				result.push( moves[i] > 0
					? map[side - 1][moves[i] - 1]
					: -map[side - 1][(Math.abs(moves[i]) - 1)] );
			}
			
			return result.slice(); // test slice
		}
	}
}