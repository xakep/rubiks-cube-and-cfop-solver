function Optimizer()
{
	return {
		// drops unnecessary moves
		optimize: function(moves) {
			if (moves.length == 0)
				return [];

			var result = [];
			result.push(moves[0]);

			for (var i = 1; i < moves.length; i++)
			{
				if (result[result.length - 1] + moves[i] == 0)
				{
					result.pop();
					continue;
				}

				if (result.length > 1)
				{
					if (result[result.length - 2] == result[result.length - 1] &&
						result[result.length - 1] == moves[i])
					{
						result.pop();
						result.pop();
						result.push(-moves[i]);
						continue;
					}
				}

				result.push(moves[i]);
			}

			return result;
		}
	}
}