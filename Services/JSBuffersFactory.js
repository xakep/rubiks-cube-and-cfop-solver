function JSBuffersFactory(constsService, layerService, cubeCoordinatesLoader, cube, colorsChangeService)
{
	// dependencies
	var _constantsService = constsService;
	var _layerService = layerService;
	var _cubeCoordinatesLoader = cubeCoordinatesLoader;
	var _cube = cube; // entire cube (already initialized)
	var _colorsChangeService = colorsChangeService;
	
	// keeps normals, they are static
	var normalsBuffer = [];
	var normalsLength = 0;
	// keeps additional colors for detailed cubes, static
	var additionalColors = [];

	// private methods
	
	// returns [plane, number] of rotation layer
	var getRotatingLayer = function(number)
	{
		return [_layerService.getPlane(number), _layerService.getNumber(number)];
	}
	
	// gets vertices buffer for specified small cube
	var getVerticesBuffer = function(smallCubeNumber)
	{
		return _cube.getSmallCube(smallCubeNumber).getVerticesBuffer();
	}
	
	// creates additional gray colors buffer for jumpers and corners
	// jumper has 4 vertices, corner has 6
	var getAdditionalSidesColorBuffer = function() {
		if (additionalColors.length == 0)
		{
			var color = _constantsService.getColor(0); // gray
			
			// 12 jumpers, 4 verts each = 48 verts
			// + 8 corners, 6 verts each = 48 verts
			// -> 96 verts
			for (var y = 0; y < 96; y++)
			{
				additionalColors.push.apply(additionalColors, color);
			}
		}

		return additionalColors;
	}

	var getColorsBuffer = function(smallCubeNumber)
	{
		var sidesColors = _colorsChangeService.getColorsObject(smallCubeNumber).getColorsBuffer();

		return _constantsService.isCubeDetailed()
			? sidesColors.concat(getAdditionalSidesColorBuffer())
			: sidesColors;
	}
	
	// returns increased indices (for static/rotating part)
	var getIndices = function(cubesCount)
	{
		var verticesCount = _cubeCoordinatesLoader.getVerticesCount();
		var indices = _cubeCoordinatesLoader.getIndices();
		var result = [];
		
		for (var i = 0; i < cubesCount; i++)
		{
			for (var y = 0; y < indices.length; y++)
			{
				result.push(indices[y] + (i * verticesCount));
			}
		}
		
		// set numItems
		result.numItems = cubesCount * _constantsService.getTrianglesPerCube();
		
		return result;
	}
	
	// creates normals buffer for entire cube
	// executes once cause this buffer is static
	var createNormalsBuffer = function()
	{
		var normalsCount = _constantsService.getVerticesPerSide(); // vertices count = normals count

		if (normalsBuffer.length == 0)
		{
			var cubeSize = _constantsService.getCubeSize();
			
			var normals = _cubeCoordinatesLoader.getNormals();
			// each vertex has a normal
			// so, one cube side has N similar normals
			normalsLength = normals.length * normalsCount;
			
			// there is no reason to keep normals for ALL cubes
			// one of the middle layers can be omitted
			var maxStaticCubes = cubeSize * cubeSize * 2 +
				(cubeSize - 2 - 1) * _constantsService.getMiddleLayerSize();
				
			var oneCubeNormals = [];
			
			// normals for one cube
			for (var i = 0; i < 6; i++) // 6 sides
			{
				for (var y = 0; y < normalsCount; y++) // N vertices per side
				{
					 // each normal has 3 floats
					oneCubeNormals.push.apply(oneCubeNormals, normals.slice(i * 3, i * 3 + 3));
				}
			}

			if (_constantsService.isCubeDetailed())
			{
				// 12 jumpers, 4 verts each, each vert has 3 floats => 12 * 4 * 3 = 144
				// 8 corners, 6 verts each => 8 * 6 * 3 = 144
				normalsLength += 288;

				oneCubeNormals.push.apply(oneCubeNormals, createAdditionalNormalsBuffer());
			}

			for (var i = 0; i < maxStaticCubes; i++)
			{
				normalsBuffer.push.apply(normalsBuffer, oneCubeNormals);
			}
		}
	}

	// for one small cube
	var createAdditionalNormalsBuffer = function()
	{
		var result = [];

		var normals = _cubeCoordinatesLoader.getAdditionalNormals(); // 60 (20 * 3)

		for (var i = 0; i < 20; i++) // 20 additional normals (12 jumpers and 8 corners)
		{
			var verts = i < 12 ? 4 : 6; // 4 vertices per jumper, 6 per corner
			for (var y = 0; y < verts; y++)
			{
				 // each normal has 3 floats
				result.push.apply(result, normals.slice(i * 3, i * 3 + 3));
			}
		}

		return result;
	}
	
	// public interface
	
	return {
		// creates JS vertices buffer for rotating part of cube
		createRotatingVerticesBuffer: function(layerNumber) {
			var rotatingVertices = [];
			var rotatingLayer = getRotatingLayer(layerNumber);
			var smallCubes = _cube.getLayer(rotatingLayer[0], rotatingLayer[1]);

			for (var i = 0; i < smallCubes.length; i++)
			{
				rotatingVertices.push.apply(rotatingVertices, getVerticesBuffer(smallCubes[i]));
			}

			return rotatingVertices;
		},
		
		// creates JS vertices buffer for static part of cube
		createStaticVerticesBuffer: function(layerNumber) {
			var staticVertices = [];
			var cubeSize = _constantsService.getCubeSize();
			var rotatingLayer = getRotatingLayer(layerNumber);

			for (var i = 0; i < cubeSize; i++)
			{
				if (i == rotatingLayer[1])
					continue; // skip rotating layer
					
				var smallCubes = _cube.getLayer(rotatingLayer[0], i);

				for(var y = 0; y < smallCubes.length; y++)
				{
					staticVertices.push.apply(staticVertices, getVerticesBuffer(smallCubes[y]));
				}
			}
			
			return staticVertices;
		},

		// creates vertices for cube frame
		createFrameVertices: function() {
			var result = [];

			for (var i = 0; i < 26; i++)
			{
				result.push.apply(result, _cube.getSmallCube(i).getFrameVerticesBuffer() );
			}

			return result;
		},

		// creates 54 vertice buffers
		createStickersVertices: function() {
			var result = [];

			for (var i = 0; i < 26; i++)
			{
				result.push.apply(result, _cube.getSmallCube(i).getStickersVerticesBuffers() );
			}

			return result;
		},

		//=========================================================================================
		
		// creates JS colors buffer for rotating part
		createRotatingColorsBuffer: function(layerNumber) {
			var rotatingColors = [];
			var rotatingLayer = getRotatingLayer(layerNumber);
			var smallCubes = _cube.getLayer(rotatingLayer[0], rotatingLayer[1]);

			for (var i = 0; i < smallCubes.length; i++)
			{
				rotatingColors.push.apply(rotatingColors, getColorsBuffer(smallCubes[i]));
			}
			
			return rotatingColors;
		},
		
		// creates JS colors buffer for static part
		createStaticColorsBuffer: function(layerNumber) {
			var staticColors = [];
			var cubeSize = _constantsService.getCubeSize();
			var rotatingLayer = getRotatingLayer(layerNumber);

			for (var i = 0; i < cubeSize; i++)
			{
				if (i == rotatingLayer[1])
					continue; // skip rotating layer
					
				var smallCubes = _cube.getLayer(rotatingLayer[0], i);

				for(var y = 0; y < smallCubes.length; y++)
				{
					staticColors.push.apply(staticColors, getColorsBuffer(smallCubes[y]));
				}
			}

			return staticColors;
		},

		// returns frame colors buffer
		createFrameColors: function() {
			var result = [];

			var color = _constantsService.getColor(0); // gray
			
			var sideColors = [];
			for (var y = 0; y < 8; y++)
			{
				sideColors = sideColors.concat(color);
			}

			for (var i = 0; i < 26; i++)
			{
				var frame = _cube.getSmallCube(i).getFrameSides();
				for (var y = 0; y < frame.length; y++)
				{
					result.push.apply(result, sideColors);
				}

				// append additional colors
				result.push.apply(result, getAdditionalSidesColorBuffer() );
			}

			return result;
		},

		// returns 54 elements
		// each one is sticker colors buffer
		createStickersColors: function() {
			var result = [];

			var color = _constantsService.getColor(0); // gray
			
			var sideColors = [];
			for (var y = 0; y < 8; y++)
			{
				sideColors.push.apply(sideColors, color);
			}

			for (var i = 0; i < 26; i++)
			{
				var stickers = _cube.getSmallCube(i).getStickerSides(); // 1, 2 or 3
				for (var y = 0; y < stickers.length; y++)
				{
					result.push.apply(result, sideColors);
				}
			}

			return result;
		},

		// creates color buffer for one side
		createStickerColors: function(colorIndex) {
			var color = _constantsService.getColor(colorIndex);
			
			var sideColors = [];
			for (var y = 0; y < 8; y++)
			{
				sideColors.push.apply(sideColors, color);
			}

			return sideColors;
		},

		//=========================================================================================
		
		// creates JS indices buffer for rotating part
		// TODO: get rid of getting small cubes array
		createRotatingIndicesBuffer: function(layerNumber) {
			var rotatingLayer = getRotatingLayer(layerNumber);
			var smallCubes = _cube.getLayer(rotatingLayer[0], rotatingLayer[1]);

			return getIndices(smallCubes.length);
		},
		
		// creates JS indices buffer for static part
		createStaticIndicesBuffer: function(layerNumber) {
			var cubeSize = _constantsService.getCubeSize();
			var rotatingLayer = getRotatingLayer(layerNumber);
			
			var cubesCount = 0;
			
			for (var i = 0; i < cubeSize; i++)
			{
				if (i == rotatingLayer[1])
					continue; // skip rotating layer
					
				cubesCount += _cube.getLayer(rotatingLayer[0], i).length;
			}
			
			return getIndices(cubesCount);
		},

		// returns frame indices buffer
		getFrameIndices: function() {
			var indices = _cubeCoordinatesLoader.getIndices();

			var result = [];
			var add = 0;

			for (var i = 0; i < 26; i++)
			{
				var frame = _cube.getSmallCube(i).getFrameSides();
				for (var y = 0; y < frame.length; y++)
				{
					// each side has 6 triangles, each has 3 indices, 18 total
					var start = frame[y] * 18;
					var a = indices.slice(start, start + 18);

					var shift = (frame[y] - y) * 8; // 8 vertices per side

					for (var n = 0; n < a.length; n++)
					{
						result.push(a[n] + add - shift);
					}
				}

				// append additional indices
				var a = indices.slice(18 * 6);
				for (var n = 0; n < a.length; n++)
				{
					result.push( a[n] + add - ((6 - frame.length) * 8) );
				}

				add += _cube.getSmallCube(i).getFrameVerticesCount();
			}

			// set numItems
			result.numItems = result.length / 3;

			return result;
		},

		// returns 54 elements
		// each one is sticker indices buffer
		getStickersIndices: function() {
			// each sticker has 18 indices (6 triangles * 3 indices)
			var indices = _cubeCoordinatesLoader.getIndices();

			var result = [];

			for (var i = 0; i < 26; i++)
			{
				var stickers = _cube.getSmallCube(i).getStickerSides();
				for (var y = 0; y < stickers.length; y++) // 1, 2 or 3
				{
					stickerInidices = indices.slice(0, 18);
					stickerInidices.numItems = 6; // 6 triangles per sticker side
					result.push(stickerInidices);
				}
			}

			return result;
		},

		//=========================================================================================
		
		// creates JS normals buffer for rotating part
		createRotatingNormalsBuffer: function(layerNumber) {
			// creates normalsBuffer if it still doesn't exist
			createNormalsBuffer();
			
			var cubeSize = _constantsService.getCubeSize();
			var fullLayerSize = cubeSize * cubeSize;
			
			var rotatingLayer = getRotatingLayer(layerNumber);
			
			if (rotatingLayer[1] == 0 || rotatingLayer[1] == cubeSize - 1) // full layer
			{
				return normalsBuffer.slice(0, fullLayerSize * normalsLength);
			}
			else // middle layer
			{
				return normalsBuffer.slice(0, _constantsService.getMiddleLayerSize() * normalsLength);
			}
		},
		
		// creates JS normals buffer for static part
		createStaticNormalsBuffer: function(layerNumber) {
			// creates normalsBuffer if it still doesn't exist
			createNormalsBuffer();
			
			var cubeSize = _constantsService.getCubeSize();
			var fullLayerSize = cubeSize * cubeSize;
			
			var rotatingLayer = getRotatingLayer(layerNumber);
			
			if (rotatingLayer[1] == 0 || rotatingLayer[1] == cubeSize - 1) // full layer
			{
				var cubes = fullLayerSize + ((cubeSize - 2) * _constantsService.getMiddleLayerSize());
				return normalsBuffer.slice(0, cubes * normalsLength);
			}
			else // middle layer
			{
				return normalsBuffer;
			}
		},

		// returns frame normals buffer
		getFrameNormals: function() {
			var result = [];

			var normals = _cubeCoordinatesLoader.getNormals();

			for (var i = 0; i < 26; i++)
			{
				var frame = _cube.getSmallCube(i).getFrameSides();
				for (var y = 0; y < frame.length; y++)
				{
					var start = frame[y] * 3;
					for (var n = 0; n < 8; n++) // 8 vertices per side
					{
						result.push.apply(result, normals.slice(start, start + 3) );
					}
				}

				// append additional normals
				result.push.apply(result, createAdditionalNormalsBuffer() );
			}

			return result;
		},

		// returns array of 54 elements
		// each one is normals buffer for sticker
		getStickersNormals: function() {
			var result = [];

			var normals = _cubeCoordinatesLoader.getNormals();

			for (var i = 0; i < 26; i++)
			{
				var stickers = _cube.getSmallCube(i).getStickerSides();
				for (var y = 0; y < stickers.length; y++)
				{
					var start = stickers[y] * 3;
					var stickerNormals = [];
					for (var n = 0; n < 8; n++) // 8 vertices per side
					{
						stickerNormals.push.apply(stickerNormals, normals.slice(start, start + 3) );
					}

					result.push( stickerNormals );
				}
			}

			return result;
		}
	}
}