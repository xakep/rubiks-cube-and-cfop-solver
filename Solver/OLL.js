function OLL(ollHelper, binarySearchFactory, rotationService, mappingService)
{
	var _ollHelper = ollHelper;
	var _binarySearchFactory = binarySearchFactory;
	var _rotationService = rotationService;
	var _mappingService = mappingService;

	// mapping between OLL algorithms and their binary representations (sorted for binary search)
	const hashes = [
		2934,				// 2
		3445,				// 1
		44425,				// 49
		60225,				// 6
		72500,				// 17
		113928,				// 36
		125698,				// 31
		126209,				// 43
		142449,				// 55
		142505,				// 52
		166486,				// 48
		166997,				// 53
		171156,				// 10
		183430,				// 7
		224289,				// 46
		264557,				// 4
		264630,				// 3
		268660,				// 18
		321921,				// 37
		330037,				// 19
		350500,				// 20
		370954,				// 42
		370961,				// 29
		514052,				// 28
		533362,				// 50
		538032,				// 5
		550306,				// 11
		558421,				// 56
		558477,				// 51
		561996,				// 14
		562580,				// 15
		574277,				// 16
		574854,				// 13
		578884,				// 34
		603432,				// 35
		627476,				// 40
		656501,				// 54
		656557,				// 47
		660076,				// 8
		672357,				// 9
		696914,				// 22
		697425,				// 21
		701000,				// 26
		713858,				// 27
		717440,				// 23
		766480,				// 25
		794985,				// 12
		799088,				// 44
		799144,				// 32
		811362,				// 38
		823636,				// 45
		823692,				// 33
		835973,				// 39
		860458,				// 41
		860465,				// 30
		905476,				// 57
		962696,				// 24
	];

	return {
		// returns moves to solve OLL stage
		solveOLL: function(cubeColors) {
			if (_ollHelper.isOLLSolved(cubeColors))
			{
				return new StageSolution([], cubeColors);
			}

			// init binary searcher
			var searcher = _binarySearchFactory.create();
			searcher.init(hashes);

			// make 4 (or less) rotations to determine the oll case
			// ollHelper.getAlgorithmNumber returns int
				
			var rotatedCubeColors = null;
				
			var a = -1; // found hash
			var i = 0;
			for (i = 0; i < 4; i++)
			{
				a = searcher.indexOf(_ollHelper.getAlgorithmNumber(
					rotatedCubeColors == null ? cubeColors : rotatedCubeColors));
				if (a == -1)
				{
					// rotate
					rotatedCubeColors = _rotationService.applyMoves(
						rotatedCubeColors == null ? cubeColors : rotatedCubeColors, [6]);
				}
				else
				{
					break;
				}
			}

			if (a != -1)
			{
				var moves = i == 0
					? _ollHelper.getMoves(a)
					: _mappingService.mappedMoves(_ollHelper.getMoves(a), i);

				return new StageSolution(
					moves,
					_rotationService.applyMoves(cubeColors, moves));
			}

			console.error("OLL error: algorithm not found"); // should not be executed at all
			return -1; // error
		}
	}
}