function CrossSolverHelper()
{
	// these are edges on the middle Y layer (X & Z colors)
	var edgesY = [F01, F21, B01, B21];
	
	// middle Z
	var edgesZ = [M22, M02, M20, M00];
	
	// middle X
	var edgesX = [B12, B10, F12, F10];
	
	// looks through all possible 12 positions
	// returns cube position [0..11]
	var findSolutionForCube = function(cubeColors) {
	
		var bottomColor = cubeColors.getSideColor(Y, M10);
		var frontColor = cubeColors.getSideColor(Z, F11);
			
		// Y edges
		for (var i = 0; i < edgesY.length; i++)
		{
			if (cubeColors.getSideColor(Z, edgesY[i]) == frontColor &&
				cubeColors.getSideColor(X, edgesY[i]) == bottomColor)
			{
				return solutions[i * 2];
			}
			
			if (cubeColors.getSideColor(X, edgesY[i]) == frontColor &&
				cubeColors.getSideColor(Z, edgesY[i]) == bottomColor)
			{
				return solutions[i * 2 + 1];
			}
		}
		
		// Z edges
		for (var i = 0; i < edgesZ.length; i++)
		{
			if (cubeColors.getSideColor(Y, edgesZ[i]) == frontColor &&
				cubeColors.getSideColor(X, edgesZ[i]) == bottomColor)
			{
				return solutions[8 + i * 2];
			}
			
			if (cubeColors.getSideColor(X, edgesZ[i]) == frontColor &&
				cubeColors.getSideColor(Y, edgesZ[i]) == bottomColor)
			{
				return solutions[8 + i * 2 + 1];
			}
		}
		
		// X edges
		for (var i = 0; i < edgesX.length; i++)
		{
			if (cubeColors.getSideColor(Y, edgesX[i]) == frontColor &&
				cubeColors.getSideColor(Z, edgesX[i]) == bottomColor)
			{
				return solutions[16 + i * 2];
			}
			
			if (cubeColors.getSideColor(Z, edgesX[i]) == frontColor &&
				cubeColors.getSideColor(Y, edgesX[i]) == bottomColor)
			{
				return solutions[16 + i * 2 + 1];
			}
		}
	}
	
	const solutions = [
		// Y edges
		[9],					// 0, front left cube
		[5, -9, -5],			// 1
		
		[-9],					// 2, front right
		[-5, 9, 5],				// 3
		
		[1, 1, 9, 1, 1],		// 4, back left
		[5, 9, -5],				// 5
		
		[3, 3, -9, 3, 3],		// 6, back right
		[-5, -9, 5],			// 7
		
		// Z edges
		[3, -9, -3],			// 8, top right
		[-6, 9, 9],				// 9
		
		[1, 9, -1],				// 10, top left
		[6, 9, 9],				// 11
		
		[-3, -9],				// 12, bottom right
		[-3, -5, 9, 5],			// 13
		
		[-1, 9],				// 14, bottom left
		[-1, 5, -9, -5],		// 15
		
		// X
		[6, 1, 9, -1],			// 16, back top
		[6, 6, 9, 9],			// 17
		
		[-7, 5, 9, -5],			// 18, back bottom
		[-7, 5, 5, -9, 5, 5],	// 19
		
		[9, 5, -9, -5],			// 20, front top
		[9, 9],					// 21
		
		[-9, 5, -9, -5],		// 22, front bottom
		[]						// 23
	];
	
	return {
		getCorrectOrientedCubesCount: function(cubeColors) {
			var c = 0; // correct cubes count
		
			var centerColor = cubeColors.getSideColor(Y, M10); // bottom center color
			
			// check 4 cubes
			// front
			if (cubeColors.getSideColor(Y, F10) == centerColor &&
				cubeColors.getSideColor(Z, F10) == cubeColors.getSideColor(Z, F11)) c++;
				
			// left
			if (cubeColors.getSideColor(Y, M00) == centerColor &&
				cubeColors.getSideColor(X, M00) == cubeColors.getSideColor(X, M01)) c++;
				
			// back
			if (cubeColors.getSideColor(Y, B10) == centerColor &&
				cubeColors.getSideColor(Z, B10) == cubeColors.getSideColor(Z, B11)) c++;
				
			// right
			if (cubeColors.getSideColor(Y, M20) == centerColor &&
				cubeColors.getSideColor(X, M20) == cubeColors.getSideColor(X, M21)) c++;

			return c;
		},
		
		// find the algorithm to place front cross cube correctly
		findAlgorithm: function(cubeColors) {
			return findSolutionForCube(cubeColors);
		}
	}
}