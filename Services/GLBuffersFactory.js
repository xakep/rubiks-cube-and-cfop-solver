function GLBuffersFactory()
{
	return {
		// creates GL buffers for vertices and colors (float)
		createFloatBuffer: function(vertices)
		{
			var buffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
			gl.bufferData(
				gl.ARRAY_BUFFER,
				new Float32Array(vertices),
				gl.STATIC_DRAW);
			
			return buffer;
		},
		
		// creates GL buffers for indices (uint)
		createUintBuffer: function(indices)
		{
			var buffer = gl.createBuffer();
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer);
			gl.bufferData(
				gl.ELEMENT_ARRAY_BUFFER,
				new Uint16Array(indices),
				gl.STATIC_DRAW);
				
			// set numItems
			buffer.numItems = indices.numItems;
			
			return buffer;
		}
	}
}