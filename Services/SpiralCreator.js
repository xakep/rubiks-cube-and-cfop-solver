function SpiralCreator(constantsService, binarySearchFactory)
{
	var binarySearcher;
	
	// each cube has 3 coordinates
	// to determine where in Z layer should be placed cube
	// with coordinates [x, y], binary search in this array
	// for X layer it's [y, z]
	// for Y layer it's [z, x]
	// will be returned position in the spiral
	// array has exactly full layer size
	var positionsInSpiral = [];
	
	// determines whether the specified layer is a middle
	var isMiddleLayer = function(layerNumber, cubeSize)
	{
		return layerNumber > 0 && layerNumber < cubeSize - 1
			? true
			: false;
	}
	
	return {
		// creates a spiral
		// in this case layer number is a Z coordinate
		create: function(layerNumber)
		{
			var startX = -1;
			var startY = -1;

			/*
			 _ _ _ _ _
			|_|_|_|_|_|
				|_|
				|_|
			 ->_ _ _|_|
			  |_|_|_|_|
			  
			*/
			
			var cubeSize = constantsService.getCubeSize();
			// for middle layers only outer cubes should be created
			// so 'half' should be equal 1 (see cycle)
			var half = isMiddleLayer(layerNumber, cubeSize)
				? 1
				: constantsService.getHalfCube();
			
			var result = [];
			
			// it creates spiral layer
			for (var i = 0; i < half; i++)
			{
				var spiral = [];
				startX++; // on first step it's = 0
				startY++; //
				
				for (var y = 0; y < 4; y++) // 4 lines always
				{
					// cubes in line: 4 for 5x5, 2 for 3x3, ...
					for (var c = 0; c < (cubeSize - 1) - (2 * i); c++) //
					{
						switch (y)
						{
							case 0: startX++; break;
							case 1: startY++; break;
							case 2: startX--; break;
							case 3: startY--; break;
						}
						
						spiral.push([startX, startY, layerNumber]);
					}
				}
				
				// put last pushed cube to the begin of array
				spiral.unshift(spiral.pop());
				
				// concat with result
				result = result.concat(spiral);
			}
			
			// there is no center in the EVEN cube
			if (!constantsService.isCubeEven())
			{
				// add the center if this is odd cube
				// and layer is full
				if (!isMiddleLayer(layerNumber, cubeSize))
				{
					result.push([half, half, layerNumber]);
				}
			}
				
			// if it is first layer, save positions to array
			// and sort it
			// later, when you need to know where in the spiral
			// should be placed cube with specified coordinates
			// just binary search in this array and you are in :)
			if (layerNumber == 0)
			{
				for (var i = 0; i < result.length; i++)
					positionsInSpiral.push([result[i][0], result[i][1], i]); // crutches
					
				positionsInSpiral.sort();
			}
			
			return result;
		},
		
		// finds position in spiral for specified coordinates
		getCubePositionInSpiral: function(cubeCoords)
		{
			if (!binarySearcher)
			{
				binarySearcher = binarySearchFactory.create();
				binarySearcher.init(positionsInSpiral);
			}
			
			return binarySearcher.indexOf(cubeCoords);
		},
	}
}