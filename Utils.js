function Utils(shaderCreator)
{
	var _shaderCreator = shaderCreator;
	
	// this is ctor
	var requestAnimFrame = function()
	{
		return window.requestAnimationFrame ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame ||
			window.oRequestAnimationFrame ||
			window.msRequestAnimationFrame ||
			function(callback, element) {
				window.setTimeout(callback, 1000 / 60);
			};
	}();
	
	return {
		// creates GL context from canvas.
		getGLContext: function(canvas) {
			if (!canvas)
			{
				return null;
			}
			
			var names = ["webgl", "experimental-webgl", "webkit-3d", "moz-webgl"];
			
			for (var i = 0; i < names.length; ++i)
			{
				try
				{
					ctx = canvas.getContext(names[i]);
				} 
				catch(e) { }
				
				if (ctx) break;
			}
			
			if (!ctx)
			{
				return null;
			}
			else
			{
				ctx.viewportWidth = canvas.width;
				ctx.viewportHeight = canvas.height;
				return ctx;
			}
		},
		
		// initializes gl program
		initProgram: function(gl) {
			var fsh = _shaderCreator.createShader(gl, "shader-fs");
			var vsh = _shaderCreator.createShader(gl, "shader-vs");
			
			if (fsh == null || vsh == null)
			{
				console.log("Check the shaders, one of them is not correct.");
			}
			
			prg = gl.createProgram();
			gl.attachShader(prg, fsh);
			gl.attachShader(prg, vsh);
			
			gl.linkProgram(prg);
			
			if (!gl.getProgramParameter(prg, gl.LINK_STATUS))
			{
				return null;
			}
			
			gl.useProgram(prg);
			
			prg.vertexPosition = gl.getAttribLocation(prg, "aVertexPosition");
			gl.enableVertexAttribArray(prg.vertexPosition);
			
			prg.vertexColor = gl.getAttribLocation(prg, "aVertexColor");
			gl.enableVertexAttribArray(prg.vertexColor);
			
			prg.vertexNormal = gl.getAttribLocation(prg, "aVertexNormal");
			gl.enableVertexAttribArray(prg.vertexNormal);
			
			prg.pMatrixUniform = gl.getUniformLocation(prg, "uPMatrix");
			prg.mvMatrixUniform = gl.getUniformLocation(prg, "uMVMatrix");
			prg.nMatrixUniform = gl.getUniformLocation(prg, "uNMatrix");
			prg.uPickingColor = gl.getUniformLocation(prg, "uPickingColor");
			prg.uOffScreen = gl.getUniformLocation(prg, "uOffScreen");
			
			return prg;
		},
		
		//
		requestAnimFrame: function(func) {
			requestAnimFrame(func);
		}
	}
}