function F2LHelper()
{
	// adds some moves to the begin of specified algorithm
	// this is instead of orientation top edge in some cases :)
	// later solution optimizer will clean this mess up
	var unshiftAlgorithm = function(algorithm, count) {
		var result = algorithm.slice();

		for (var i = 0; i < count; i++)
			result.unshift(6);
			
		return result;
	}
	
	// http://www.speedsolving.com/wiki/index.php/F2L#Reposition_Edge
	// this array should be freezed
	const algorithms = [
		[6, 9, -6, -9], 							// F2L 2+
		[3, 3, 7, -6, -7, 6, 3, 3],					// F2L 6+
		[9, 6, -1, 6, 6, 1, 6, -9],					// F2L 8+	// 2nd
		[-3, 6, 3, -6, -6, 9, 6, -9],				// F2L 16+
		
		[-6, 9, 6, 6, -9, 6, -3, -6, 3],			// F2L 12+
		[6, -3, -6, 3, -6, -3, -6, 3],				// F2L 10+
		[-3, -6, 3],								// F2L 4+	// 6th
		[6, -3, 6, 3, -6, -3, -6, 3],				// F2L 14+
		
		[6, -3, 6, 6, 3, -6, -3, -6, 3],			// F2L 34+
		[-6, 9, 6, -9, 6, -3, -6, 3],				// F2L 36+
		
		//
		
		[-6, 9, -6, -9, 6, 9, 6, -9],				// F2L 13+
		[9, 6, -9],									// F2L 3+
		[-6, 9, 6, -9, 6, 9, 6, -9],				// F2L 9+	// 12th
		[6, -3, 6, 6, 3, -6, 9, 6, -9],				// F2L 11+
		
		[2, -6, 1, 9, -1, 6, -2],					// F2L 15+
		[-3, -6, 7, 6, 6, -7, -6, 3],				// F2L 7+
		[9, 9, -1, 6, 1, -6, 9, 9],					// F2L 5+	// 16th
		[-6, -3, 6, 3],								// F2L 1+
		
		[6, -3, 6, 3, 6, 6, -3, 6, 3],				// F2L 33+
		[6, -3, -6, 3, -6, 9, 6, -9],				// F2L 35+
		
		//
		
		[9, 6, 6, -9, -6, 9, 6, -9],				// F2L 18+
		[6, 9, 6, 6, 9, 9, 3, 9, -3],				// F2L 20+
		[9, -1, 6, 6, 1, -9],						// F2L 22+	// 22th
		[-6, 9, -1, -6, 1, -9, -3, -6, 3],			// F2L 24+
		
		[-6, -9, 3, 9, -3, -6, -3, -6, 3],			// F2L 23+
		[-3, 7, 6, 6, -7, 3],						// F2L 21+
		[-6, -3, 6, 6, 3, 3, -9, -3, 9],			// F2L 19+	// 26th
		[-3, 6, 6, 3, 6, -3, -6, 3],				// F2L 17+
		
		[-6, -3, 6, 7, 6, 6, -7, -6, 3],			// F2L 32+
		[-3, 6, 3, -6, 9, -6, -9],					// F2L 31+
		
		//
		
		[-6, -3, 6, 3, 6, 9, -6, -9],				// F2L 26+	// 30th
		[6, 3, 9, -3, -6, -3, 6, 3, -9],			// F2L 25+	// 31th
		
		[3, 3, 6, 6, -9, 3, 3, 9, 6, 6, 3, -6, 3],	// F2L 38+
		
		//
		
		[3, -9, -3, 9, 3, -9, -3, 9],				// F2L 29+	// 33th
		[6, -3, 6, 3, -6, -3, 6, 3],				// F2L 27+	// 34th
		
		[-3, -9, -6, -3, 6, 3, 9, 6, 3],			// F2L 41+
		[-3, 6, 3, 9, 9, -1, 6, 1, -6, 9, 9],		// F2L 39+
		
		//
		[9, -6, 9, 9, 3, 9, -3],					// F2L 28+	// 37th
		[-6, 9, -6, -9, -3, -6, 3],					// F2L 30+	// 38th
		
		[-3, -6, -9, -3, -6, 3, 6, 9, 3],			// F2L 42+
		[-3, 6, 3, -6, -3, 6, 6, 3, -6, -3, 6, 3]	// F2L 40+
	];
	
	const topEdges = [F12, M02, B12, M22];
	const last3edges = [F01, B01, B21];
	
	// these moves are additional for last3edges
	// they placed edge to the B12 position
	// this is 4 and 8 algorithms for first corner
	const prependMoves = [
		[6, -1, -6, 1], // F01
		[6, 1, -6, -1], // B01
		[3, 6, -3],		// B21
	];
	
	// these moves are additional when
	// corner is in the bottom layer
	const prependMoves2 = [
		[-1, 6, 1],
		[1, 6, -1],
		[3, -6, -3]
	];
	
	const corners = [F22, F20, F02, B22, B02, F00, B20, B00];
	const cornersPrependMoves = [
		[6],				// F02
		[-6],				// B22
		[6, 6],				// B02
		[-1, 6, 1],			// F00
		[7, -6, -7],		// B20
		[1, -6, -1, -6],	// B00
	];

	// makes algorithms immutable
	var freezeAlgorithms = function() {
		for (var i = 0; i < algorithms.length; i++)
			Object.freeze(algorithms[i]);

		Object.freeze(algorithms);
	}

	// returns -1 or -2 if corner for slot is placed correctly
	// [0..5] otherwise
	var findCornerPosition = function(cubeColors) {
		var bottomColor = cubeColors.getSideColor(Y, M10);
		var frontColor = cubeColors.getSideColor(Z, F11);
		var rightColor = cubeColors.getSideColor(X, M21);

		var c = [bottomColor, frontColor, rightColor].sort();
		
		for (var i = 0; i < corners.length; i++)
		{
			var cornerColors = [
				cubeColors.getSideColor(Y, corners[i]),
				cubeColors.getSideColor(Z, corners[i]),
				cubeColors.getSideColor(X, corners[i]),
			];
			
			cornerColors.sort();
			
			if (c[0] == cornerColors[0] && c[1] == cornerColors[1] && c[2] == cornerColors[2])
			{
				return i - 2; // first 2 corners are good
			}
		}
	}
	
	return {
		// finds algorithm to close current (front right) slot
		findAlgorithm: function(cubeColors) {
		
			if (Object.isFrozen(algorithms) == false)
			{
				freezeAlgorithms();
			}
		
			var bottomColor = cubeColors.getSideColor(Y, M10);
			var frontColor = cubeColors.getSideColor(Z, F11);
			var rightColor = cubeColors.getSideColor(X, M21);
			
			// [0..9]
			if (cubeColors.getSideColor(Z, F22) == frontColor &&
				cubeColors.getSideColor(Y, F22) == rightColor &&
				cubeColors.getSideColor(X, F22) == bottomColor)
			{
				// corner is on the place, check the edge
				// first 4 algorithms
				for (var i = 0; i < topEdges.length; i++)
				{
					if (cubeColors.getSideColor(Y, topEdges[i]) == rightColor)
					{
						if (cubeColors.getSideColor(i % 2 == 0 ? Z : X, topEdges[i]) == frontColor)
						{
							return algorithms[i];
						}
					}
					else if (cubeColors.getSideColor(Y, topEdges[i]) == frontColor)
					{
						if (cubeColors.getSideColor(i % 2 == 0 ? Z : X, topEdges[i]) == rightColor)
						{
							return algorithms[4 + i];
						}
					}
				}
				
				// edge is on the place
				if (cubeColors.getSideColor(Z, F21) == frontColor &&
					cubeColors.getSideColor(X, F21) == rightColor)
				{
					return algorithms[8];
				}
				
				if (cubeColors.getSideColor(X, F21) == frontColor &&
					cubeColors.getSideColor(Z, F21) == rightColor)
				{
					return algorithms[9];
				}
				
				// check additional edges
				for (var i = 0; i < last3edges.length; i++)
				{
					if (cubeColors.getSideColor(Z, last3edges[i]) == frontColor &&
						cubeColors.getSideColor(X, last3edges[i]) == rightColor)
					{
						return prependMoves[i].concat(algorithms[6]); // F2L 4
					}
					
					if (cubeColors.getSideColor(Z, last3edges[i]) == rightColor &&
						cubeColors.getSideColor(X, last3edges[i]) == frontColor)
					{
						return prependMoves[i].concat(algorithms[2]); // F2L 8
					}
				}
			}
			
			// [10..19]
			if (cubeColors.getSideColor(Y, F22) == frontColor &&
				cubeColors.getSideColor(X, F22) == rightColor &&
				cubeColors.getSideColor(Z, F22) == bottomColor)
			{
				for (var i = 0; i < topEdges.length; i++)
				{
					if (cubeColors.getSideColor(Y, topEdges[i]) == rightColor)
					{
						if (cubeColors.getSideColor(i % 2 == 0 ? Z : X, topEdges[i]) == frontColor)
						{
							return algorithms[10 + i];
						}
					}
					else if (cubeColors.getSideColor(Y, topEdges[i]) == frontColor)
					{
						if (cubeColors.getSideColor(i % 2 == 0 ? Z : X, topEdges[i]) == rightColor)
						{
							return algorithms[14 + i];
						}
					}
				}
				
				// edge is on the place
				if (cubeColors.getSideColor(Z, F21) == frontColor &&
					cubeColors.getSideColor(X, F21) == rightColor)
				{
					return algorithms[18];
				}
				
				if (cubeColors.getSideColor(X, F21) == frontColor &&
					cubeColors.getSideColor(Z, F21) == rightColor)
				{
					return algorithms[19];
				}
				
				// check additional edges
				for (var i = 0; i < last3edges.length; i++)
				{
					if (cubeColors.getSideColor(Z, last3edges[i]) == frontColor &&
						cubeColors.getSideColor(X, last3edges[i]) == rightColor)
					{
						return prependMoves[i].concat(algorithms[16]); // F2L 5
					}
					
					if (cubeColors.getSideColor(Z, last3edges[i]) == rightColor &&
						cubeColors.getSideColor(X, last3edges[i]) == frontColor)
					{
						return prependMoves[i].concat(algorithms[12]); // F2L 9
					}
				}
			}
			
			// [20..29]
			if (cubeColors.getSideColor(X, F22) == frontColor &&
				cubeColors.getSideColor(Z, F22) == rightColor &&
				cubeColors.getSideColor(Y, F22) == bottomColor)
			{
				for (var i = 0; i < topEdges.length; i++)
				{
					if (cubeColors.getSideColor(Y, topEdges[i]) == rightColor)
					{
						if (cubeColors.getSideColor(i % 2 == 0 ? Z : X, topEdges[i]) == frontColor)
						{
							return algorithms[20 + i];
						}
					}
					else if (cubeColors.getSideColor(Y, topEdges[i]) == frontColor)
					{
						if (cubeColors.getSideColor(i % 2 == 0 ? Z : X, topEdges[i]) == rightColor)
						{
							return algorithms[24 + i];
						}
					}
				}
				
				// edge is on the place
				if (cubeColors.getSideColor(Z, F21) == frontColor &&
					cubeColors.getSideColor(X, F21) == rightColor)
				{
					return algorithms[28];
				}
				
				if (cubeColors.getSideColor(X, F21) == frontColor &&
					cubeColors.getSideColor(Z, F21) == rightColor)
				{
					return algorithms[29];
				}
				
				// check additional edges
				for (var i = 0; i < last3edges.length; i++)
				{
					if (cubeColors.getSideColor(Z, last3edges[i]) == frontColor &&
						cubeColors.getSideColor(X, last3edges[i]) == rightColor)
					{
						return prependMoves[i].concat(algorithms[26]); // F2L 19
					}
					
					if (cubeColors.getSideColor(Z, last3edges[i]) == rightColor &&
						cubeColors.getSideColor(X, last3edges[i]) == frontColor)
					{
						return prependMoves[i].concat(algorithms[22]); // F2L 22
					}
				}
			}
			
			//
			
			if (cubeColors.getSideColor(Z, F20) == frontColor &&
				cubeColors.getSideColor(X, F20) == rightColor &&
				cubeColors.getSideColor(Y, F20) == bottomColor)
			{
				for (var i = 0; i < topEdges.length; i++)
				{
					if (cubeColors.getSideColor(Y, topEdges[i]) == rightColor &&
						cubeColors.getSideColor(i % 2 == 0 ? Z : X, topEdges[i]) == frontColor)
					{
						return unshiftAlgorithm(algorithms[30], i);
					}
					else if (cubeColors.getSideColor(Y, topEdges[i]) == frontColor &&
						cubeColors.getSideColor(i % 2 == 0 ? Z : X, topEdges[i]) == rightColor)
					{
						return unshiftAlgorithm(algorithms[31], i);
					}
				}
				
				// edge can be on it's own place
				if (cubeColors.getSideColor(Z, F21) == rightColor &&
					cubeColors.getSideColor(X, F21) == frontColor)
				{
					return algorithms[32];
				}
				
				// if solved 37 algorithm is required, place it here
				
				for (var i = 0; i < last3edges.length; i++)
				{
					if (cubeColors.getSideColor(Z, last3edges[i]) == frontColor &&
						cubeColors.getSideColor(X, last3edges[i]) == rightColor)
					{
						return prependMoves2[i].concat(algorithms[31]); // F2L 25
					}
					
					if (cubeColors.getSideColor(Z, last3edges[i]) == rightColor &&
						cubeColors.getSideColor(X, last3edges[i]) == frontColor)
					{
						return prependMoves2[i].concat(algorithms[30]); // F2L 26
					}
				}
			}
			
			if (cubeColors.getSideColor(X, F20) == frontColor &&
				cubeColors.getSideColor(Y, F20) == rightColor &&
				cubeColors.getSideColor(Z, F20) == bottomColor)
			{
				for (var i = 0; i < topEdges.length; i++)
				{
					if (cubeColors.getSideColor(Y, topEdges[i]) == rightColor &&
						cubeColors.getSideColor(i % 2 == 0 ? Z : X, topEdges[i]) == frontColor)
					{
						return unshiftAlgorithm(algorithms[33], i);
					}
					else if (cubeColors.getSideColor(Y, topEdges[i]) == frontColor &&
						cubeColors.getSideColor(i % 2 == 0 ? Z : X, topEdges[i]) == rightColor)
					{
						return unshiftAlgorithm(algorithms[34], i);
					}
				}
				
				// edge can be on it's own place
				if (cubeColors.getSideColor(Z, F21) == rightColor &&
					cubeColors.getSideColor(X, F21) == frontColor)
				{
					return algorithms[35];
				}
				
				if (cubeColors.getSideColor(X, F21) == rightColor &&
					cubeColors.getSideColor(Z, F21) == frontColor)
				{
					return algorithms[36];
				}
				
				//
				for (var i = 0; i < last3edges.length; i++)
				{
					if (cubeColors.getSideColor(Z, last3edges[i]) == frontColor &&
						cubeColors.getSideColor(X, last3edges[i]) == rightColor)
					{
						return prependMoves2[i].concat(algorithms[34]); // F2L 27
					}
					
					if (cubeColors.getSideColor(Z, last3edges[i]) == rightColor &&
						cubeColors.getSideColor(X, last3edges[i]) == frontColor)
					{
						return prependMoves2[i].concat(algorithms[33]); // F2L 29
					}
				}
			}
			
			if (cubeColors.getSideColor(Y, F20) == frontColor &&
				cubeColors.getSideColor(Z, F20) == rightColor &&
				cubeColors.getSideColor(X, F20) == bottomColor)
			{
				for (var i = 0; i < topEdges.length; i++)
				{
					if (cubeColors.getSideColor(Y, topEdges[i]) == rightColor &&
						cubeColors.getSideColor(i % 2 == 0 ? Z : X, topEdges[i]) == frontColor)
					{
						return unshiftAlgorithm(algorithms[37], i);
					}
					else if (cubeColors.getSideColor(Y, topEdges[i]) == frontColor &&
						cubeColors.getSideColor(i % 2 == 0 ? Z : X, topEdges[i]) == rightColor)
					{
						return unshiftAlgorithm(algorithms[38], i);
					}
				}
				
				// edge can be on it's own place
				if (cubeColors.getSideColor(Z, F21) == rightColor &&
					cubeColors.getSideColor(X, F21) == frontColor)
				{
					return algorithms[39];
				}
				
				if (cubeColors.getSideColor(X, F21) == rightColor &&
					cubeColors.getSideColor(Z, F21) == frontColor)
				{
					return algorithms[40];
				}
				
				//
				for (var i = 0; i < last3edges.length; i++)
				{
					if (cubeColors.getSideColor(Z, last3edges[i]) == frontColor &&
						cubeColors.getSideColor(X, last3edges[i]) == rightColor)
					{
						return prependMoves2[i].concat(algorithms[38]); // F2L 30
					}
					
					if (cubeColors.getSideColor(Z, last3edges[i]) == rightColor &&
						cubeColors.getSideColor(X, last3edges[i]) == frontColor)
					{
						return prependMoves2[i].concat(algorithms[37]); // F2L 28
					}
				}
			}
			
			// place the corner
			// and search again
			console.error("ERROR: ALGORITHM NOT FOUND!");
			return -1; // algorithm not found, probably corner or edge are placed on the another slot right now
		},

		// check whether the current slot is closed
		// returns true is slot is closed
		// false otherwise
		isSlotClosed: function(cubeColors) {
			// check the edge (it is smaller :)
			if (cubeColors.getSideColor(Z, F21) != cubeColors.getSideColor(Z, F11) ||
				cubeColors.getSideColor(X, F21) != cubeColors.getSideColor(X, M21))
			{
				return false; // opened
			}

			// check the corner
			if (cubeColors.getSideColor(Y, F20) != cubeColors.getSideColor(Y, M10) ||
				cubeColors.getSideColor(Z, F20) != cubeColors.getSideColor(Z, F11) ||
				cubeColors.getSideColor(X, F20) != cubeColors.getSideColor(X, M21))
			{
				return false; // opened
			}

			return true; // closed
		},

		// returns moves which should be done to place the corner correctly
		// empty array if corner is already in place
		getMovesToPlaceCorner: function(cubeColors) {
			var cornerPosition = findCornerPosition(cubeColors);
			if (cornerPosition < 0)
				return [];
			return cornersPrependMoves[cornerPosition];
		},
	}
}
