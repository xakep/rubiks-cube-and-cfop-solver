const X = 0;
const Y = 1;
const Z = 2;

// back layer
const B00 = 0;
const B10 = 1;
const B20 = 2;
const B21 = 3;
const B22 = 4;
const B12 = 5;
const B02 = 6;
const B01 = 7;
const B11 = 8;


// middle layer
const M00 = 9;
const M10 = 10;
const M20 = 11;
const M21 = 12;
const M22 = 13;
const M12 = 14;
const M02 = 15;
const M01 = 16;

// front layer
const F00 = 17;
const F10 = 18;
const F20 = 19;
const F21 = 20;
const F22 = 21;
const F12 = 22;
const F02 = 23;
const F01 = 24;
const F11 = 25;