// keeps colors and colorsBuffer for one small cube
function SmallCubeColors(colors, colorsBuffer)
{
	var _colors;
	var _colorsBuffer;
	
	// constructor
	var _ctor = function() {
		_colors = colors;
		_colorsBuffer = colorsBuffer;
    }()
	
	return {
		// returns colors, [-1, 0, 5] for example
		getColors: function() {
			return _colors;
		},
		
		// returns buffer
		getColorsBuffer: function() {
			return _colorsBuffer;
		}
	}
}