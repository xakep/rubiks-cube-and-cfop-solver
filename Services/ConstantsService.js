// Provides constants for the whole project.
function ConstantsService()
{
	// Private vars.
	
	// Holds the cube size.
	var cubeSize = -1;
	var halfCube = -1;
	var layerSize = -1;
	var middleLayerSize = -1;
	var isCubeEven = false;
	var detailedCubeSize = 5; // starting from this size cube is not detailed anymore

	// vars to separate detailed/simple cubes
	var verticesPerSide = -1; // (regular side) 4 for simple, 8 for detailed
	var trianglesPerCube = -1; // 12 (2 * 6) for simple, 92 (6 * 6 + 12 * 2 + 8 * 4) for detailed
	
	// colors
	var cubeColors = [
		// back sides
		[0.5, 0.5, 0.5, 1.0],  // gray		0
		// sides
		[1.0, 0.0, 0.0, 1.0], // red		1
		[1.0, 0.5, 0.0, 1.0], // orange		2
		[1.0, 1.0, 0.0, 1.0], // yellow		3
		[1.0, 1.0, 1.0, 1.0], // white		4
		[0.0, 1.0, 0.0, 1.0], // lime		5
		[0.0, 0.0, 1.0, 1.0], // blue		6
		[0.85, 0.85, 0.85, 1.0], // for tests only
	];
	
	// ===================================================================
	
	// Private methods.
	
	// initializes a variables
	// (after cubeSize is set).
	var init = function()
	{
		halfCube = Math.floor(cubeSize / 2);
		layerSize = cubeSize * cubeSize;
		middleLayerSize = calculateMiddleLayerSize(cubeSize);
		isCubeEven = cubeSize % 2 == 0;

		verticesPerSide = cubeSize < detailedCubeSize ? 8 : 4;
		trianglesPerCube = cubeSize < detailedCubeSize ? 92 : 12;
	}
	
	var calculateMiddleLayerSize = function(size) {
		return (size - 1) * 4;
	}
	
	// ===================================================================
	
	// Public methods.
	
	return {
	
		// Gets cube size.
		getCubeSize: function() { return cubeSize; },

		// returns max limit of detailed cube
		getDetailedCubeLimit: function() { return detailedCubeSize; },
		
		// Sets cube size.
		setCubeSize: function(size)
		{
			cubeSize = size;
			init();
		},
		
		// Gets cube half size.
		getHalfCube: function() { return halfCube; },
		
		// Gets the full layer size.
		getLayerSize: function() { return layerSize; },
		
		// Gets the middle layer size.
		getMiddleLayerSize: function() { return middleLayerSize; },
		
		// Gets the middle layer size for specified cube size.
		getMiddleLayerSizeFor: function(cubeSize) { return calculateMiddleLayerSize(cubeSize); },
		
		// Get color by id
		getColor: function(number) { return cubeColors[number]; },
		
		// Returns a value indicating whether the cube is even
		isCubeEven: function() { return isCubeEven; },
		
		// Returns an opposite color for the specified one.
		getOppositeColor: function(colorNumber)
		{
			return colorNumber % 2 == 0
				? colorNumber - 1
				: colorNumber + 1;
		},

		// returns vertices per one side
		getVerticesPerSide: function()
		{
			return verticesPerSide;
		},

		// returns triangles per one small cube
		getTrianglesPerCube: function()
		{
			return trianglesPerCube;
		},

		// returns TRUE if cubeSize < 5
		isCubeDetailed: function()
		{
			return cubeSize < detailedCubeSize ? true : false;
		}
	}
}