// represents colors for all 3x3 cube
// used by solver
function CubeColors()
{
	// array of 26 arrays
	var colors = null;
	
	return {
		// initializes the object
		init: function(cubesColors) {
			colors = cubesColors;
		},
		
		// returns color of the specified cube side
		getSideColor: function(side, cubeNumber) {
			return Math.abs( colors[cubeNumber][side] );
		},
		
		// returns all colors as array
		getColors: function() {
			return colors;
		}
	}
}