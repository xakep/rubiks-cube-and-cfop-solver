function InteractiveRenderer()
{
	var artist;
	var picker;
	var sceneObjects;
	var selectedColor = 1; // red by default
	var cubeDesiredPosition;
	var _glBuffersFacade;

	var renderLoop = function() {
		if (artist != null) // artist is null when renderer is released
		{
			utils.requestAnimFrame(renderLoop);

			if (cubeDesiredPosition != null) // for solver
			{
				var sign = cubeDesiredPosition.getDegree() > 0 ? -1 : 1;
				cubeDesiredPosition.getAxis() == 0 // X
					? artist.changeSceneXAngle(sign)
					: artist.changeSceneYAngle(sign);

				var degree = cubeDesiredPosition.updateDegree(sign);
				if (degree == 0)
					cubeDesiredPosition = null; // stop
			}

			// draw
			artist.drawScene();
		}
	}

	return {
		run: function(glBuffersFacade, randomLayerService) {
			_glBuffersFacade = glBuffersFacade;
			artist = new Artist(glBuffersFacade, randomLayerService);
			picker = new Picker();
			artist.init(picker);

			sceneObjects = glBuffersFacade.getFrameAndStickers();

			renderLoop();
		},

		getCanvasCoordinates: function(ev, canvas) {
			var x, y, top = 0, left = 0;
			var obj = canvas;

			while (obj && obj.tagName !== 'BODY') {
				top += obj.offsetTop;
				left += obj.offsetLeft;
				obj = obj.offsetParent;
			}
			
			left += window.pageXOffset;
			top -= window.pageYOffset;
		 
			// return relative mouse position
			x = ev.clientX - left;
			y = canvas.height - (ev.clientY - top);
			
			picked = picker.find({ x:x, y:y }, sceneObjects);
			if (picked != -1)
			{
				if (picked != 0) // one of the stickers hit, 0 is frame
				{
					if (ev.ctrlKey) // ctrl button
					{
						_glBuffersFacade.updateSceneObject(picked, 0);

						// show solve button if all stickers are set
						if (!_glBuffersFacade.areAllStickersSet())
						{
							$('#solveBtn').fadeOut(200);
						}
					}
					else
					{
						_glBuffersFacade.updateSceneObject(picked, selectedColor);

						// show solve button if all stickers are set
						if (_glBuffersFacade.areAllStickersSet())
						{
							$('#solveBtn').fadeIn(200);
						}
					}
				}
			}
		},

		// position's type is CubeDesiredPosition
		rotateCubeToPosition: function(position) {
			cubeDesiredPosition = position;
		},

		// zooms the view, +1 - in; -1 - out
		zoom: function(delta) {
			artist.zoom(delta);
		},

		setSelectedColor: function(colorIndex) {
			selectedColor = colorIndex;
		},

		// returns colors which user set
		getColors: function() {
			var colors = [];

			for (var i = 1; i < sceneObjects.length; i++)
			{
				colors.push(sceneObjects[i].colorIndex);
			}

			return colors;
		},

		release: function() {
			artist = null;
		},

		// rotates the cube around X, step is 5.0 ('a' key)
		rotateCube: function() {
			artist.increaseSceneAngle();
		},

		solveCube: function(solution) {
			// unused
		}
	}
}