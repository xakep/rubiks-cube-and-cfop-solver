// CCW -> +3
// CW -> -3
// rotationBottomLayer -> CW (default)
const CW = 0;
const CCW = 1;

const FRONT_SIDE = 0;
const RIGHT_SIDE = 1;
const BACK_SIDE = 2;
const LEFT_SIDE = 3;
const TOP_SIDE = 4;
const BOTTOM_SIDE = 5;

// for PLL patterns
const NONE = 0;
const LEFT = 1;
const RIGHT = 2;
const BOTH = 3;