function SmallCubeInitializer(
	smallCubeFactory,
	constantsService,
	smallCubeColorsService,
	colorsChangeService)
{
	return {
		initializeSmallCube: function(coordinates, colors, coloredStickers) // [x, y, z]
		{
			var halfCube = constantsService.getHalfCube() - (constantsService.isCubeEven() ? 0.5 : 0);
			
			// should create empty small cube via factory
			var smallCube = smallCubeFactory.create();

			// and initialize it with correct coordinates
			var x = coordinates[0] - halfCube;
			var y = coordinates[1] - halfCube;
			var z = coordinates[2] - halfCube;

			smallCube.create([x, y, z]);

			// create and save colors for this cube
			var smallCubeColors = colors == null
				? smallCubeColorsService.createColorsObject([x, y, z])
				: smallCubeColorsService.createColorsObject(
					[x, y, z],
					colors.slice(coloredStickers, coloredStickers + smallCube.getStickerSides().length));

			colorsChangeService.pushColorsObject(smallCubeColors);
			
			return smallCube;
		},
	};
}