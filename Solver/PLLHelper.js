function PLLHelper(constantsService)
{
	var _constantsService = constantsService;

	// http://www.speedsolving.com/wiki/index.php/PLL
	const algorithms = [
		[2, 2, -6, -2, 6, 6, 2, -6, 2, 2], // Ua http://sarah.cubing.net/images/pll-recog-guide/2SPLL-Ua1.png
		[7, 7, -6, 2, 6, 6, -2, -6, 7, 7], // Ub http://sarah.cubing.net/images/pll-recog-guide/2SPLL-Ub1.png
		[2, 2, -6, 2, 2, 6, 6, 2, 2, -6, 2, 2], // 2 H (full mirror) http://sarah.cubing.net/images/pll-recog-guide/2SPLL-H1.png
		[2, 2, 4, 8, 8, -4, 8, 2, 2, -8], // Z http://sarah.cubing.net/images/pll-recog-guide/2SPLL-Z1.png
		[-7, -3, -7, 1, 1, 7, 3, -7, 1, 1, 7, 7], // Aa http://sarah.cubing.net/images/pll-recog-guide/2SPLL-Aa1.png
		[1, 9, 1, 7, 7, -1, -9, 1, 7, 7, 1, 1], // 5 Ab http://sarah.cubing.net/images/pll-recog-guide/2SPLL-Ab1.png
		[3, -6, -1, 4, 4, 1, 6, -3, -1, -6, 3, 4, 4, -3, 6, 1], // E http://sarah.cubing.net/images/pll-recog-guide/2SPLL-E1.png
		[3, -6, -3, 6, 3, 3, 9, 6, -9, -6, -3, -9, 3, 9, 3, 3, 6], // F http://sarah.cubing.net/images/pll-recog-guide/2SPLL-F1.png
		[7, 7, 4, -1, -6, -1, 6, 1, -4, 7, 7, 3, -6, -3], // 8 Ga http://sarah.cubing.net/images/pll-recog-guide/2SPLL-Ga4.png
		[9, 6, -9, 3, 3, 4, -7, -6, 7, 6, 7, -4, 3, 3], // Gb http://sarah.cubing.net/images/pll-recog-guide/2SPLL-Gb1.png
		[1, 1, -4, 7, 6, 7, -6, -7, 4, 1, 1, -9, 6, 9], // Gc http://sarah.cubing.net/images/pll-recog-guide/2SPLL-Gc1.png
		[-3, -6, 3, 9, 9, -4, 1, 6, -1, -6, -1, 4, 9, 9], // Gd http://sarah.cubing.net/images/pll-recog-guide/2SPLL-Gd4.png
		[1, 6, 6, -1, -6, -9, 3, 9, 1, -9, -3, 9, -6, -1, -6], // 12 Ra http://sarah.cubing.net/images/pll-recog-guide/2SPLL-Ra1.png
		[-3, -9, -6, 9, -3, -9, 6, -3, -6, -3, 6, 3, 9, 3, 3], // Rb http://sarah.cubing.net/images/pll-recog-guide/2SPLL-Rb4.png
		[3, -6, -1, 6, 6, -3, 6, 3, 6, 6, -3, 1], // Ja http://sarah.cubing.net/images/pll-recog-guide/2SPLL-Ja1.png
		[-9, 7, 6, 6, -7, 6, 7, 6, 6, 9, -6, -7], // Jb http://sarah.cubing.net/images/pll-recog-guide/2SPLL-Jb1.png
		[-3, 6, 1, 6, 6, 3, -6, -1, -3, 6, 1, 6, 6, 3, -6, -1], // 16 Na http://sarah.cubing.net/images/pll-recog-guide/2SPLL-Na1.png
		[-1, -6, 3, 6, 6, 1, 6, -3, -1, -6, 3, 6, 6, 1, 6, -3], // Nb http://sarah.cubing.net/images/pll-recog-guide/2SPLL-Nb1.png
		[7, -6, -7, 6, -7, -3, 7, 7, 6, -7, 6, 7, -6, -7, 3], // T http://sarah.cubing.net/images/pll-recog-guide/2SPLL-T1.png
		[-7, -6, -7, 6, -1, -7, 1, 1, 6, -1, -6, -1, 7, 1, 7], // V http://sarah.cubing.net/images/pll-recog-guide/2SPLL-V1.png
		[-9, -3, 6, 3, 6, -3, -6, -9, -6, 9, 3, -9, 6, 9, 9], // Y http://sarah.cubing.net/images/pll-recog-guide/2SPLL-Y1.png
	];

	return {
		// returns moves for specified algorithm number
		getPLLAlgorithm: function(number) {
			return algorithms[number];
		},

		// returns true if both sided are 3x1 blocks
		isPLLSolved: function(cubeColors) {
			var cubes = [ [F12, F02], [M22, B22] ];

			for (var i = 0; i < 2; i++)
			{
				var color = cubeColors.getSideColor(i == 0 ? Z : X, F22);

				for (var y = 0; y < 2; y++)
				{
					if (color != cubeColors.getSideColor(i == 0 ? Z : X, cubes[i][y]))
						return false;
				}
			}

			return true;
		},

		// returns true if left side has a 3x1 block
		is3x1OnTheLeft: function(cubeColors) {
			var color = cubeColors.getSideColor(Z, F22);
			if (color != cubeColors.getSideColor(Z, F12) || color != cubeColors.getSideColor(Z, F02))
				return false;

			return true;
		},

		// returns true if specified side has a headlights
		hasSideHeadlights: function(cubeColors, side) {
			var cubes = [ [F02, F12], [B22, M22] ];

			if (cubeColors.getSideColor(side == 0 ? Z : X, F22) != cubeColors.getSideColor(side == 0 ? Z : X, cubes[side][0]))
				return false;

			if (cubeColors.getSideColor(side == 0 ? Z : X, F22) == cubeColors.getSideColor(side == 0 ? Z : X, cubes[side][1]))
				return false;

			return true;
		},

		// returns true if sticker between headlights has an opposite color
		hasHeadlightsOppositeColor: function(cubeColors, side) {
			var middleCubes = [F12, M22];
			var oppositeColor = _constantsService.getOppositeColor(cubeColors.getSideColor(
				side == FRONT_SIDE ? Z : X, F22));

			if (oppositeColor != cubeColors.getSideColor(side == FRONT_SIDE ? Z : X, middleCubes[side]))
				return false;

			return true;
		},

		// returns true if specified side has an inner 2x1 block
		hasSideInnerBlock: function(cubeColors, side) {
			var middleCubes = [ F12, M22 ];
			var backCubes = [ F02, B22 ];

			if (cubeColors.getSideColor(side == FRONT_SIDE ? Z : X, F22) != 
				cubeColors.getSideColor(side == FRONT_SIDE ? Z : X, middleCubes[side]))
				return false;

			if (cubeColors.getSideColor(side == FRONT_SIDE ? Z : X, F22) == 
				cubeColors.getSideColor(side == FRONT_SIDE ? Z : X, backCubes[side]))
				return false;

			return true;
		},

		// returns true if on the side with inner 2x1 block outer sticker has an opposite color
		hasOuterCornerOppositeColor: function(cubeColors, side) {
			var outerCorners = [F02, B22];
			var oppositeColor = _constantsService.getOppositeColor(
				cubeColors.getSideColor(side == FRONT_SIDE ? Z : X, F22));

			if (oppositeColor != cubeColors.getSideColor(side == FRONT_SIDE ? Z : X, outerCorners[side]))
				return false;

			return true;
		},

		// returns true if specified side has an outer block
		hasSideOuterBlock: function(cubeColors, side) {
			var cubes = [ [F12, F02], [M22, B22] ];

			if (cubeColors.getSideColor(side == FRONT_SIDE ? Z : X, cubes[side][0]) !=
				cubeColors.getSideColor(side == FRONT_SIDE ? Z : X, cubes[side][1]))
				return false;

			if (cubeColors.getSideColor(side == FRONT_SIDE ? Z : X, cubes[side][0]) ==
				cubeColors.getSideColor(side == FRONT_SIDE ? Z : X, F22))
				return false;

			return true;
		},

		// returns true if on the side with outer 2x1 block inner sticker has an opposite color
		hasInnerCornerOppositeColor: function(cubeColors, side) {
			var middleCubes = [F12, M22];
			var oppositeColor = _constantsService.getOppositeColor(
				cubeColors.getSideColor(side == FRONT_SIDE ? Z : X, middleCubes[side]));

			if (oppositeColor != cubeColors.getSideColor(side == FRONT_SIDE ? Z : X, F22))
				return false;

			return true;
		},

		// returns true if left side has a checker parrern
		hasLeftSideCheckerPattern: function(cubeColors) {
			if (cubeColors.getSideColor(Z, F12) != cubeColors.getSideColor(X, F22) ||
				cubeColors.getSideColor(Z, F22) != cubeColors.getSideColor(Z, F02))
				return false;

			return true;
		},

		// returns true if right side has a checker parrern
		hasRightSideCheckerPattern: function(cubeColors) {
			if (cubeColors.getSideColor(X, M22) != cubeColors.getSideColor(Z, F22) ||
				cubeColors.getSideColor(X, B22) != cubeColors.getSideColor(X, F22))
				return false;

			return true;
		},

		// returns true if outer corners have same color
		haveOuterCornersSameColor: function(cubeColors) {
			return cubeColors.getSideColor(Z, F02) != cubeColors.getSideColor(X, B22)
				? false
				: true;
		},

		isThereCheckerPattern: function(cubeColors) {
			if (cubeColors.getSideColor(Z, F12) == cubeColors.getSideColor(X, F22) &&
				cubeColors.getSideColor(Z, F22) == cubeColors.getSideColor(X, M22))
			{
				return true;
			}

			if (cubeColors.getSideColor(Z, F02) == cubeColors.getSideColor(X, M22) &&
				cubeColors.getSideColor(Z, F12) == cubeColors.getSideColor(X, B22))
			{
				return true;
			}

			return false;
		},

		// returns true if it so
		// used by distinguish E cases
		haveLeftOuterCornerAndRightEdgeSameColor: function(cubeColors) {
			return cubeColors.getSideColor(Z, F02) == cubeColors.getSideColor(X, M22)
				? true
				: false;
		}
	}
}