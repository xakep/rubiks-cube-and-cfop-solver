function F2L(rotationService, f2lSlotSolver)
{
	var _rotationService = rotationService;
	var _f2lSlotSolver = f2lSlotSolver;
	
	return {
		// solves F2L stage by closing all opened slots
		solveF2L: function(cubeColors) {
			var result = []; // moves to close all opened slots
			var updatedCubeColors = null;

			var openedSlotsCount = 4 - _f2lSlotSolver.getClosedSlotsCount(cubeColors); // what if 2 slots will be closed at once?
			for (var i = 0; i < openedSlotsCount; i++)
			{
				var moves = _f2lSlotSolver.closeSlot(i == 0 ? cubeColors : updatedCubeColors);
				result = result.concat( moves );

				// applyMoves
				updatedCubeColors = _rotationService.applyMoves(i == 0 ? cubeColors : updatedCubeColors, moves);
			}

			return new StageSolution(result, updatedCubeColors == null ? cubeColors : updatedCubeColors);
		}
	}
}