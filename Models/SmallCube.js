// SmallCube.
function SmallCube(cubeCoordinatesLoader)
{
	// updated positions
	var vertexPositionBuffer = [];
	// keeps cube coordinates
	var position;
	// keeps frame sides indexes
	var frameSides = []; // [0..5]
	var stickerSides = [];

	var frameVertices = [];
	var stickersVertices = [];
	
	return {
		// creates initialized small cube.
		create: function(coords)
		{
			position = coords;
			// coords look like this: [0, 0, -1] - center on back side

			var vertices = cubeCoordinatesLoader.getVertices();
			// update vertices according to specified coordinates
			for (var c = 0; c < vertices.length; c += 3)
			{
				vertexPositionBuffer.push(vertices[c + 0] + coords[0]);
				vertexPositionBuffer.push(vertices[c + 1] + coords[1]);
				vertexPositionBuffer.push(vertices[c + 2] + coords[2]);
			}

			// sides order is (see json):
			// first 8 * 3 vertices is a right side
			// next is a left side
			// top side
			// bottom side
			// front side
			// back side
			for (var i = 0; i < 3; i++)
			{
				if (coords[i] == 0) // both sides are frame
				{
					frameSides.push(i * 2);
					frameSides.push(i * 2 + 1);
					continue;
				}

				if (coords[i] > 0) // +
				{
					frameSides.push(i * 2 + 1); // opposite (negative) side is a frame
					stickerSides.push(i * 2);
				}
				else
				{
					frameSides.push(i * 2); // positive side is a frame
					stickerSides.push(i * 2 + 1);
				}
			}
		},
		
		// returns updated JS vertices.
		getVerticesBuffer: function()
		{
			return vertexPositionBuffer;
		},

		// returns vertices for frame.
		getFrameVerticesBuffer: function()
		{
			// check the position
			// if only one coordinate is [1] this is a center
			// two are [1] - edge
			// three are [1] - corner

			for (var i = 0; i < 3; i++) // 0 - x, 1 - y, 2 - z
			{
				if (Math.abs(position[i]) == 1) // outer, get one of the sides (+/-)
				{
					// each side has 24 vertices (3 * 8)

					var start = i * 48;

					if (position[i] > 0) // +
					{
						start += 24;
					}

					frameVertices = frameVertices.concat( vertexPositionBuffer.slice(start, start + 24) );

					// add stickers
					start = i * 48;

					if (position[i] < 0) // +
					{
						start += 24;
					}

					stickersVertices.push( vertexPositionBuffer.slice(start, start + 24) );
				}
				else
				{
					// get both sides
					var start = i * 48; // 48 = 24 * 2 = (3 * 8) * 2
					frameVertices = frameVertices.concat( vertexPositionBuffer.slice(start, start + 48) );
				}
			}

			// append additional sides
			frameVertices = frameVertices.concat( vertexPositionBuffer.slice(48 * 3) );

			return frameVertices; // frame for this cube
		},

		// returns vertices count in frame mode
		getFrameVerticesCount: function() {
			return frameVertices.length / 3;
		},

		// returns 1, 2 or 3 buffers according to cube position.
		getStickersVerticesBuffers: function()
		{
			return stickersVertices;
		},

		// returns indexes for frame sides
		getFrameSides: function()
		{
			return frameSides;
		},

		getStickerSides: function() {
			return stickerSides;
		}
	};
}