// Service update colors after each rotation.
function ColorsChangeService(constantsService, smallCubeColorsService, cubeColorsFactory)
{
	var colors = []; // SmallCubeColors objects
	
	return {
		// saves specified color object in the colors array
		pushColorsObject: function(color) {
			colors.push(color);
		},
		
		// updates colors after rotation
		// numbers are positions in the colors array of the rotated cubes
		updateColors: function(direction, rotationPlane, numbers) {
			var cubeSize = constantsService.getCubeSize() - 1;
			var middleLayerSize = constantsService.getMiddleLayerSize();
			var halfCube = constantsService.getHalfCube();
			
			// this is for cubes bigger than 3x3
			// I need to know from which position starts next inner layer
			// this var keeps amount of all cubes containing in the previous layers
			var allPreviousLayersCubes = 0;
			
			// this is to save first N cubes
			var tmp = [];
			
			if (direction == 0) // clockwise, -> -, direction is 0
			{
				for (var y = 0; y < halfCube; y++)
				{
					// if it is one of the middle layers of cube
					// only outer cubes should be replaced
					if (y > 0 && numbers.length == middleLayerSize) break;
					
					// inner cube size
					var innerCubeSize = cubeSize - 2 * y;
					// get new middle size (size was decreased by 1 above)
					var innerMiddleLayerSize = constantsService.getMiddleLayerSizeFor(innerCubeSize + 1);
					
					tmp = [];
					
					for (var i = 0; i < innerMiddleLayerSize; i++)
					{
						if (tmp.length < innerCubeSize)
						{
							tmp.push(colors[ numbers[i + allPreviousLayersCubes] ]);
						}
						
						colors[ numbers[i + allPreviousLayersCubes] ] = smallCubeColorsService.createColorsObjectFrom(
							colors[ numbers[i + allPreviousLayersCubes] ], // original colors object
							i < innerMiddleLayerSize - innerCubeSize
								? colors[ numbers[i + innerCubeSize + allPreviousLayersCubes] ]
								: tmp[ i + innerCubeSize - innerMiddleLayerSize ], // new one
							rotationPlane);
					}
					
					// add this layer cubes amount
					allPreviousLayersCubes += innerMiddleLayerSize;
				}
			}
			else // counterclockwise, -> -, direction is 1
			{
				for (var y = 0; y < halfCube; y++)
				{
					// if it is one of the middle layers of cube
					// only outer cubes should be replaced
					if (y > 0 && numbers.length == middleLayerSize) break;
					
					// inner cube size
					var innerCubeSize = cubeSize - 2 * y;
					// get new middle size (size was decreased by 1 above)
					var innerMiddleLayerSize = constantsService.getMiddleLayerSizeFor(innerCubeSize + 1);
					
					tmp = [];
					
					for (var i = innerMiddleLayerSize - 1; i >= 0; i--)
					{
						if (tmp.length < innerCubeSize)
						{
							tmp.unshift(colors[ numbers[i + allPreviousLayersCubes] ]);
						}
						
						colors[ numbers[i + allPreviousLayersCubes] ] = smallCubeColorsService.createColorsObjectFrom(
							colors[ numbers[i + allPreviousLayersCubes] ], // original colors object
							i < innerCubeSize
								? tmp[i]
								: colors[ numbers[i - innerCubeSize + allPreviousLayersCubes] ], // new one
							rotationPlane);
					}
					
					// add this layer cubes amount
					allPreviousLayersCubes += innerMiddleLayerSize;
				}
			}
		},
		
		// gets the color object by index
		getColorsObject: function(cubeNumber) {
			return colors[cubeNumber];
		},

		getAllColors: function() {
			var result = [];
			for (var i = 0; i < colors.length; i++)
			{
				result.push(colors[i].getColorsBuffer());
			}

			return result;
		},
		
		// this is for solver
		// creates cubeColors object of cube which should be solved
		getCubeColors: function() {
			var colorObjects = [];
			for (var i = 0; i < colors.length; i++)
			{
				colorObjects.push(colors[i].getColors());
			}
			
			return cubeColorsFactory.createCubeColors(colorObjects);
		},
		
		setSideColor: function(cubeNumber, side, color) {
			colors[cubeNumber] = smallCubeColorsService.updateSideColor(
				colors[cubeNumber].getColors(), side, color);
		}
	}
}