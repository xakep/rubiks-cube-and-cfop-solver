const sideNames = [ "Front", "Right", "Back", "Left", "Top", "Bottom" ];

$(document).ready(function() {

	$('#grid').attr('currentSide', '0');
	$( "#currentSideLbl" ).text( "Front" );

	// hover if cell is not colored yet
	$('.cell').hover(function(e) {
		if (!$(this).attr('colored'))
		{
			if (e.type == 'mouseenter')
				$(this).addClass('cellHovered');
			else
				$(this).removeClass('cellHovered');
		}
	});
	
	// click on the color cell
	$('.cell').click(function(e) {
		$('.cell').removeClass('cellSelected');
		$(this).addClass('cellSelected');

		renderer.setSelectedColor(parseInt(this.id));
	});

	// show the next side
	$('#nextBtn').click(function() {
		var currentSide = getCurrentSide() + 1; // increase current side number

		if (currentSide == 6) // next circle
		{
			currentSide = 0; // reset to front
			renderer.rotateCubeToPosition(new CubeDesiredPosition(X, -4))
		}
		else
		{
			currentSide < 5
				? renderer.rotateCubeToPosition(new CubeDesiredPosition(Y, 6)) // 0:4
				: renderer.rotateCubeToPosition(new CubeDesiredPosition(X, 4)); // 5
		}
		
		$('#grid').attr('currentSide', currentSide);
		$( "#currentSideLbl" ).text( sideNames[currentSide] );
	});

	// show the previous side
	$('#prevBtn').click(function() {
		var currentSide = getCurrentSide() - 1;

		if (currentSide == -1)
		{
			currentSide = 5; // bottom
			renderer.rotateCubeToPosition(new CubeDesiredPosition(X, 4));
		}
		else
		{
			currentSide == 4
				? renderer.rotateCubeToPosition(new CubeDesiredPosition(X, -4))
				: renderer.rotateCubeToPosition(new CubeDesiredPosition(Y, -6));
		}

		$('#grid').attr('currentSide', currentSide);
		$( "#currentSideLbl" ).text( sideNames[currentSide] );
	});

	$('#nextMove').click(function() {
		if (renderer.getState() == 0 && position < movesToSolve.length) // 0 is IDLE state
		{
			var doubleMove = movesToSolve[position + 1] == movesToSolve[position];
			renderer.applyMoves(movesToSolve.slice(position, position + (doubleMove ? 2 : 1)));
			position += doubleMove ? 2 : 1;

			// update the progress
			updateTheProgress(1);
		}
	});

	$('#prevMove').click(function() {
		if (renderer.getState() == 0 && position > 0)
		{
			var doubleMove = movesToSolve[position - 2] == movesToSolve[position - 1];
			var reversedMoves = movesToSolve.slice(doubleMove ? position - 2 : position - 1, position);
			for (var i = 0; i < reversedMoves.length; i++)
				reversedMoves[i] = -reversedMoves[i];
			renderer.applyMoves(reversedMoves);
			
			position -= doubleMove ? 2 : 1;

			// update the progress
			updateTheProgress(-1);
		}
	});

	$('#cubeSize').change(function() {
		var size = $(this).val();
		size != 3
			? $('#enableSolver').hide()
			: $('#enableSolver').show();
	});

	// show solver form
	$('#enableSolver').click(function() {
		$('#cubeSettings').hide();
		$('#grid').show();

		start(3, true); // create interactive renderer
	});

	// find solution for specified cube
	$('#solveBtn').click(function() {
		$('#grid').fadeOut(100);
		$('#solvingBtns').fadeIn(100);

		// back 3D model to default position
		switch (getCurrentSide())
		{
			case 0, 4: // front
				// do nothing
				break;

			case 1: // right
				renderer.rotateCubeToPosition(new CubeDesiredPosition(Y, -6))
				break;

			case 2: // back
				renderer.rotateCubeToPosition(new CubeDesiredPosition(Y, 12))
				break;

			case 3: // left
				renderer.rotateCubeToPosition(new CubeDesiredPosition(Y, 6))
				break;

			case 5:
				renderer.rotateCubeToPosition(new CubeDesiredPosition(X, -4))
				break;
		}

		var colors = renderer.getColors();
		start(3, false, colors);
	});

	// PRIVATE HELPERS

	// updates the progress counter
	var updateTheProgress = function(i) {
		$('#performedMovesCount').text(
			parseInt($('#performedMovesCount').text()) + i);
	}

	// returns current side number [0..5]
	var getCurrentSide = function() {
		return parseInt($('#grid').attr('currentSide'));
	};
});