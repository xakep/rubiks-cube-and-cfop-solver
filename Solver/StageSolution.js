function StageSolution(moves, cubeColors)
{
	var _moves = moves;
	var _cubeColors = cubeColors;

	return {
		getMoves: function() {
			return _moves;
		},

		getCubeColors: function() {
			return _cubeColors;
		}
	}
}