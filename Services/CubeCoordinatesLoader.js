// Loads coordinates for one small cube asynchronously.
function CubeCoordinatesLoader(loadedCallback)
{
	var vertices = null;
	var indices = null;
	var normals = null;
	var additionalNormals = null; // normals for jumpers and corners in detailed cube
	
	// loads the model
	var loadModel = function(fileName) {
		var request = new XMLHttpRequest();
		request.open("GET", fileName);

		request.onreadystatechange = function()
		{
			if (request.readyState == 4)
			{
				if (request.status == 200) 
				{
					handleLoadedModel(JSON.parse(request.responseText));
				}
			}
		}
		
		request.send();
	}
	
	// gets vertices and indices from loaded model
	var handleLoadedModel = function(model) {
		vertices = model.vertices;
		indices = model.indices;
		normals = model.normals;
		additionalNormals = model.additionalNormals;
		
		// continue
		loadedCallback();
	}
	
	return {
		// returns loaded indices
		getIndices: function() {
			return indices;
		},
		
		// returns loaded vertices
		getVertices: function() {
			return vertices;
		},
		
		// returns loaded normals
		getNormals: function() {
			return normals;
		},

		getAdditionalNormals: function() {
			return additionalNormals;
		},
		
		getVerticesCount: function() {
			return vertices.length / 3;
		},
		
		// loads the model via ajax
		load: function(fileName) {
			loadModel(fileName);
		}
	}
}